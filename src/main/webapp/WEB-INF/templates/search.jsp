<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/7/5
  Time: 17:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>搜索页面</title>
    <link href="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/user_page.css">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
<jsp:include page="./static/pagetop.jsp"></jsp:include>

<div class="page_center">

    <div class="essay_div">
        <div class="tuijian_div">
            <p>
                搜索结果
            </p>
        </div>
        <c:if test="${not empty zero_essays}">
            <div class="not_data2" style="display: block;" id="not_data2">
                <div class="div_img">
                    <img src="/css/image/nothing.png" alt="此内容不存在">
                </div>
                <div class="div_text">
                    搜索记录不存在
                </div>
            </div>
        </c:if>
        <c:forEach items="${essays}" var="essay">
            <div class="more_essay">
                <div class="essay_img">
                    <c:if test="${not empty sessionScope.userid}">
                        <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_blank"><img class="essay_photo"  src="${essay.articleImg}" alt="冰川"></a>
                    </c:if>
                    <c:if test="${empty sessionScope.userid}">
                        <a href="/essay_page?id=${essay.id}" target="_blank"><img class="essay_photo"  src="${essay.articleImg}" alt="冰川"></a>
                    </c:if>
                </div>
                <div class="essay_content">
                    <div class="essay_title">
                        <c:if test="${not empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_blank"><p>${essay.articleTitle}</p></a>
                        </c:if>
                        <c:if test="${empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}" target="_blank"><p>${essay.articleTitle}</p></a>
                        </c:if>                    </div>
                    <div class="essay_text">
                        <p>
                                ${essay.articleData}
                        </p>
                    </div>
                    <div class="essay_data">
                        <div class="author_name">
                            <p><a href="/user_data?method=user_essay&user_id=${essay.author}">${essay.authorName}</a></p>
                        </div>
                        <div class="create_time">
                            <p>${essay.gmtCreate}</p>
                        </div>
                    </div>
                </div>
                <div class="essay_phone_title">
                    <c:if test="${not empty sessionScope.userid}">
                        <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_self"><p>${essay.articleTitle}</p></a>
                    </c:if>
                    <c:if test="${empty sessionScope.userid}">
                        <a href="/essay_page?id=${essay.id}" target="_self"><p>${essay.articleTitle}</p></a>
                    </c:if>
                </div>
            </div>
        </c:forEach>
    </div>
    <div class="toast-container position-fixed bottom-0 end-0 p-3" style="z-index: 1010;">
        <div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body" style="color: #ff0000 !important;font-weight: 700">
                    请输入文章标题
                </div>
                <%--            <button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>--%>
            </div>
        </div>
    </div>
</div>
<jsp:include page="./static/pagebottom.jsp"></jsp:include>
<script>
    if (${not empty zero_essays}){
        var toastElList = [].slice.call(document.querySelectorAll('.toast'))
        var toastList = toastElList.map(function(toastEl) {
            return new bootstrap.Toast(toastEl)
        })
        $(".toast-body").html("没有搜索记录，5秒后自动返回主页");
        toastList.forEach(toast => toast.show())
        setTimeout(function() {
            window.location.href = '/';
        }, 5000);
    }
</script>
</body>
<script src="/js/essay.js"></script>
</html>