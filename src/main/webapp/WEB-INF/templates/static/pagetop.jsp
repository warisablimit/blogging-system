<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/7/5
  Time: 17:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/index2.css">
    <link rel="stylesheet" href="/css/login.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <script src="https://cdn.bootcdn.net/ajax/libs/axios/1.5.0/axios.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
<div class="page_top">
    <ul class="logo" style="margin: 0">
        <!-- <li><div class="logo_img"><img src="./css/image/简简博客logo.png" width="90%" height="90%"  alt="简简博客"></div></li> -->
        <li ><a href="/" class="logo_text" style="font-size: 30px;color: #087df6;">简简博客</a></li>
    </ul>
    <ul class="user_ul" style="margin: 0">
        <li class="search_div_li">
            <form id="searchForm" action="/search" method="post">
                <input type="text" class="search" name="keyword" style="font-size: 16px" placeholder="请输入文章标题..." onkeydown="handleEnter(event)">
                <button class="search_submit"  type="submit"></button>
            </form>
        </li>
        <c:if test="${not empty sessionScope.userid}">
            <!-- 用户已登录的情况 -->
            <li class="phone_li"><a class="phone_user_data"><img src="${sessionScope.photo}" alt="${sessionScope.username}的默认头像"></a></li>
            <li class="user_img"><a href="/userpage?id=${sessionScope.userid}"><img src="${sessionScope.photo}" alt="${sessionScope.username}的默认头像"></a></li>
            <li class="user_name_li"><a href="/userpage?id=${sessionScope.userid}">${sessionScope.username}</a></li>
            <li class="button_li1"><button class="fabu_essay" ><a href="/addessay">发布文章</a></button></li>
            <a class="logout_button" style="margin-left: 10px;" href="/logout">退出登录</a>
            <!-- 显示用户名称 -->
        </c:if>

        <c:if test="${empty sessionScope.userid}">
            <!-- 用户未登录的情况 -->
            <li class="button_li"><button class="login_top_button" id="login_top_button">登录/注册</button></li>
            <li class="button_pai"><button class="phone_login_button" id="login_top_button2">登录</button></li>
            <!-- 显示登录/注册按钮 -->
        </c:if>
    </ul>
</div>
<div class="user_data">
    <div class="user_name2">
        ${sessionScope.username}
    </div>
    <div class="user_home">
        <li><a style="color: #000000;" href="/user_data?method=user_essay&user_id=${sessionScope.userid}">个人中心</a></li>
    </div>
    <div class="log_out">
        <a href="/logout" style="color: #000000;">退出登录</a>
    </div>
<%--    <div class="search_div" style="border-bottom: 0;">--%>
<%--        --%>
<%--        <form id="searchForm">--%>
<%--            <input type="text" class="search" name="keyword">--%>
<%--            <button type="button" id="searchButton">搜索</button> <!-- 将submit按钮改成普通按钮 -->--%>
<%--        </form>--%>
<%--    </div>--%>
</div>
<div class="login_div">
    <div class="login_box_tel">
        <div class="login_box_top">
            <div class="tel_login">
                <button class="tel_login_button" type="button">登录</button>
            </div>
            <div class="password_login">
                <button class="password_login_button" type="button">注册</button>
            </div>
        </div>
        <form action="/dologin" method="post">
            <div class="login_box_center">
                <div class="tel_number_box">
                    <input class="tel_text" name="name" type="text" placeholder="请输入用户名">
                    <div class="tel_login_zhengze"></div>
                </div>
                <div class="password_number_box">
                    <input class="password" name="password" type="password" placeholder="请输入密码">
                    <div class="display_password">
                        <button class="display_password_button0" type="button"></button>
                    </div>
                    <div class="chat_data_number_zhengze"></div>
                </div>

            </div>
            <div class="login_box_footer">
                <button class="login_button" type="submit">登录</button>
            </div>
        </form>
    </div>
    <div class="login_box_pass">
        <div class="login_box_top">
            <div class="tel_login">
                <button class="tel_login_button">登录</button>
            </div>
            <div class="password_login">
                <button class="password_login_button">注册</button>
            </div>
        </div>
        <form action="/enrol" method="post">
            <div class="login_box_center">
                <div class="tel_number_box">
                    <input class="tel_email_text" name="name" type="text" placeholder="请输入注册名">
                    <div class="tel_email_login_zhengze"></div>
                </div>
                <div class="password_number_box">
                    <input class="new_password" name="password" type="password" placeholder="请输入注册密码">
                    <div class="display_password">
                        <button class="display_password_button" type="button"></button>
                    </div>
                    <div class="password_login_zhengze"></div>
                </div>
            </div>
            <div class="login_box_footer">
                <input class="login_button" value="注册" type="submit">
            </div>
        </form>
    </div>
</div>
<div class="go_top">
    <button class="go_top_button"></button>
</div>

<script>
    // function handleEnter(event) {
    //     if (event.key === 'Enter') {
    //         event.preventDefault(); // 阻止默认的提交行为
    //         search(); // 触发按钮点击事件
    //     }
    // }
    <%--function search(){--%>
    <%--    //点击后就把轮番图进行隐藏--%>
    <%--    $(".lunfan_div").css('display','none');--%>
    <%--    $(".tuijian_div").css('display','none');--%>
    <%--    var keyword = $(".search").val();--%>
    <%--    console.log("-----------------------" + keyword)--%>
    <%--    if (keyword) {--%>
    <%--        $.ajax({--%>
    <%--            type: "POST",--%>
    <%--            url: "/search",--%>
    <%--            data: { keyword: keyword },--%>
    <%--            success: function(response) {--%>
    <%--                $("#essay_div").empty(); // 清空原有的文章列表--%>
    <%--                console.log("--------------------------" + response.essays)--%>
    <%--                $.each(response.essays, function(index, essay) {--%>
    <%--                    var timestamp = essay.gmtCreate; // 假设这是时间戳--%>
    <%--                    var date = new Date(timestamp);--%>
    <%--                    var options = { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' };--%>
    <%--                    var formattedTime = date.toLocaleString('ja-JP', options).replace(/\//g, '-');--%>
    <%--                    var essay_data = '<div class="more_essay">';--%>
    <%--                    essay_data += '<div class="essay_img">';--%>
    <%--                    essay_data += '<c:if test="${not empty sessionScope.userid}">';--%>
    <%--                    essay_data += '<a href="/essay_page?id=' + essay.id + '&user_id=' + ${sessionScope.userid}  + '" target="_blank"><img class="essay_photo"  src="' + essay.articleImg + '" alt="冰川"></a>';--%>
    <%--                    essay_data += '</c:if>';--%>
    <%--                    essay_data += '<c:if test="${empty sessionScope.userid}">';--%>
    <%--                    essay_data += '<a href="/essay_page?id=' + essay.id + '" target="_blank"><img class="essay_photo"  src="' + essay.articleImg + '" alt="冰川"></a>';--%>
    <%--                    essay_data += '</c:if>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    essay_data += '<div class="essay_content">';--%>
    <%--                    essay_data += '<div class="essay_title">';--%>
    <%--                    essay_data += '<c:if test="${not empty sessionScope.userid}">';--%>
    <%--                    essay_data += '<a href="/essay_page?id=' + essay.id + '&user_id=' + ${sessionScope.userid} + '" target="_blank"><p>' + essay.articleTitle + '</p></a>';--%>
    <%--                    essay_data += '</c:if>';--%>
    <%--                    essay_data += '<c:if test="${empty sessionScope.userid}">';--%>
    <%--                    essay_data += '<a href="/essay_page?id=' + essay.id + '" target="_blank"><p>' + essay.articleTitle + '</p></a>';--%>
    <%--                    essay_data += '</c:if>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    essay_data += '<div class="essay_text">';--%>
    <%--                    essay_data += '<p>';--%>
    <%--                    essay_data += essay.articleData;--%>
    <%--                    essay_data += '</p>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    essay_data += '<div class="essay_data">';--%>
    <%--                    essay_data += '<div class="author_name">';--%>
    <%--                    essay_data += '<p><a href="/user_data?method=user_essay&user_id=' + essay.author + '">' + essay.authorName + '</a></p>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    essay_data += '<div class="create_time">';--%>
    <%--                    essay_data += '<p>' + formattedTime + '</p>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    essay_data += '<div class="essay_phone_title">';--%>
    <%--                    essay_data += '<a href="<%=request.getContextPath()%>/Essay_Content?method=select&id=' + essay.id + '" target="_blank"><p>' + essay.articleTitle + '</p></a>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    essay_data += '</div>';--%>
    <%--                    $("#essay_div").append(essay_data);--%>
    <%--                });--%>
    <%--            },--%>
    <%--            error: function() {--%>
    <%--                console.log('请求失败');--%>
    <%--                // 处理请求失败的逻辑--%>
    <%--            }--%>
    <%--        });--%>
    <%--    } else {--%>
    <%--        window.location.href = "/";--%>
    <%--    }--%>
    <%--}--%>
</script>
<%--<script type="text/javascript" src="/js/essay.js"></script>--%>
<script type="text/javascript" src="/js/login.js"></script>
</body>
</html>
