
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar" style="background-color: rgb(255,255,255)!important;box-shadow: 0px 0px 8px 3px rgba(0, 0, 0, 0.15)!important;">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">



        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header" style="background-color: rgb(255,255,255)!important;box-shadow: 0px 0px 8px 3px rgba(0, 0, 0, 0.15)!important;color: #087df6;border: 0;font-size: 18px; text-align: center;">菜单</li>

            <li class="treeview active" style="border:0!important;text-align: left;">
                <a href="#" style="background-color: rgb(255,255,255)!important;box-shadow: 0px 0px 8px 3px rgba(0, 0, 0, 0.15)!important;color: #087df6;border: 0;font-size: 18px; "><span>文章管理</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/essay_list" style="border: 0;">文章列表</a></li>
                    <li><a href="/search_page" style="border: 0;">查询文章</a></li>
                </ul>
            </li>

            <li class="treeview active" style="border:0!important;text-align: left;">
                <a href="#" style="background-color: rgb(255,255,255)!important;box-shadow: 0px 0px 8px 3px rgba(0, 0, 0, 0.15)!important;color: #087df6;border: 0;font-size: 18px; "><span>用户管理</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/user_list" style="border: 0;">用户列表</a></li>
                    <li><a href="/searchuser_byname" style="border: 0;">查询用户</a></li>
                </ul>
            </li>

            <li class="treeview active" style="border:0!important;text-align: left;">
                <a href="#" style="background-color: rgb(255,255,255)!important;box-shadow: 0px 0px 8px 3px rgba(0, 0, 0, 0.15)!important;color: #087df6;border: 0;font-size: 18px; "><span>评论管理</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/comment_list" style="border: 0;">评论列表</a></li>
                    <li><a href="/searchcomment_byessayid" style="border: 0;">通过文章查找评论</a></li>
                    <li><a href="/searchcomment_bycontent" style="border: 0;">查询评论</a></li>
                </ul>
            </li>


        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
