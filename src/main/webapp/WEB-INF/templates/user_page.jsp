<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/7/5
  Time: 17:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${user_list.name}的个人中心</title>
    <link rel="stylesheet" href="/css/user_page.css">
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/index2.css">
    <link href="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
<jsp:include page="./static/pagetop.jsp"></jsp:include>
<div class="page_center">
    <div class="user_max_div">
        <div class="user_data_div">
            <div class="user_data_img">
                <img id="previewImage2"  src="${user_list.photo}" alt="${user_list.name}的头像">
            </div>
            <div class="user_data_name">
                ${user_list.name}
            </div>
            <div class="user_data_edit">
                <c:if test="${user_list.id == sessionScope.userid}">
                    <a ><button class="edit_button">编辑</button></a>
                </c:if>

            </div>
        </div>
        <div class="user_operate">
            <div class="user_essay active" onclick="searchEssay(3)"><a class="href_essay" href="javascript:void(0)" >文章</a></div>
            <div class="user_like" onclick="searchEssay(1)"><a class="href_like" href="javascript:void(0)" >喜欢</a></div>
            <div class="user_favorite" onclick="searchEssay(2)"><a class="href_favorite" href="javascript:void(0)" >收藏</a></div>
        </div>
        <c:if test="${empty essays}">
            <div class="not_data">
                <div class="div_img">
                    <img src="/css/image/nothing.png" alt="此内容不存在">
                </div>
                <div class="div_text">
                    没有此内容
                </div>
            </div>
        </c:if>
        <c:if test="${not empty essays}">
            <div class="not_data2">
                <div class="div_img">
                    <img src="/css/image/nothing.png" alt="此内容不存在">
                </div>
                <div class="div_text">
                    没有此内容
                </div>
            </div>
        </c:if>
        <div id="essay_div" >
            <c:forEach items="${essays}" var="essay">
                <div class="more_essay">
                    <div class="essay_img">
                        <c:if test="${not empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_blank"><img class="essay_photo"  src="${essay.articleImg}" alt="冰川"></a>
                        </c:if>
                        <c:if test="${empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}" target="_blank"><img class="essay_photo"  src="${essay.articleImg}" alt="冰川"></a>
                        </c:if>
                    </div>
                    <div class="essay_content">
                        <div class="essay_title">
                            <c:if test="${not empty sessionScope.userid}">
                                <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_blank"><p>${essay.articleTitle}</p></a>
                            </c:if>
                            <c:if test="${empty sessionScope.userid}">
                                <a href="/essay_page?id=${essay.id}" target="_blank"><p>${essay.articleTitle}</p></a>
                            </c:if>
                        </div>
                        <div class="essay_text">
                            <p>
                                    ${essay.articleData}
                            </p>
                        </div>
                        <div class="essay_data">
                            <div class="author_name">
                                <p><a href="/userpage?id=${essay.author}">${essay.authorName}</a></p>
                            </div>
                            <div class="create_time">
                                <p>${essay.gmtCreate}</p>
                            </div>
                            <c:if test="${sessionScope.userid == essay.author}">
                                <div class="dropdown">
                                    <button type="button" class="dropbtn" onclick="delete_essay(${essay.id})">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                        </svg>
                                    </button>
                                    <button class="dropbtn"  onclick="update_essay(${essay.id})">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="essay_phone_title">
                        <c:if test="${not empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_self"><p>${essay.articleTitle}</p></a>
                        </c:if>
                        <c:if test="${empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}" target="_self"><p>${essay.articleTitle}</p></a>
                        </c:if>
                    </div>
                </div>
                <%--                <div class="essay_phone_title">--%>
                <%--                    <div class="essay_title_a">--%>
                <%--                        <a href="<%=request.getContextPath()%>/Essay_Content?method=select&id=${essay.id}" target="_blank"><p>${essay.articleTitle}</p></a>--%>
                <%--                    </div>--%>
                <%--                    <c:if test="${sessionScope.userid == essay.author}">--%>
                <%--                        <div class="delete_button">--%>
                <%--                            <button class="delete_caidan"></button>--%>
                <%--                        </div>--%>
                <%--                    </c:if>--%>
                <%--                </div>--%>
                <%--            <div class="delete_xiugai">--%>
                <%--                <div class="delete_button_div">--%>
                <%--                    <a href="/Essay_Content?method=updata_essay&user_id=${sessionScope.user.id}&id=${essay.id}">修改</a>--%>
                <%--                    <a href="/Essay_Content?method=delte_essay&user_id=${sessionScope.user.id}&id=${essay.id}">删除</a>--%>
                <%--                </div>--%>
                <%--            </div>--%>
            </c:forEach>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>



        <c:if test="${sessionScope.userid == user_list.id}">
            <div class="max_write">
                <form>
                    <div class="user_write_div">
                        <div class="img_div">
                            <div class="user_write_img">
                                <label for="fileInput" class="file-input-wrapper">
                                    <img id="previewImage"  src="${user_list.photo}" for="fileInput"  alt="用户头像">
                                    <input style="display: none" type="file" id="fileInput">
                                </label>
                                <textarea style="display: none;"   name="articleImg" id="base64TextArea" rows="10" cols="50"></textarea>
                            </div>

                                <%--                        <div class="write_img"><button>上传头像</button></div>--%>
                        </div>
                        <div class="user_write_name">
                            <input class="input_name" type="text" placeholder="请输入更改后的用户名.." name="name" value="${user_list.name}">
                        </div>
                        <div class="password2">
                            <input class="input_password" type="password" name="password" placeholder="请输入新密码">
                        </div>
                        <div class="user_telphone">
                            <input type="text" value="${user_list.phoneNumber}" name="tel_number" placeholder="请输入手机号">
                        </div>
                        <div class="div_submit">
                            <input class="button_submit" type="button" value="确认更改">
                        </div>
                    </div>
                </form>
            </div>
        </c:if>



    </div>
</div>
<%--        提交提示Div--%>
<div class="toast-container position-fixed bottom-0 end-0 p-3" style="z-index: 1010;">
    <div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body" style="color: #ff0000 !important;font-weight: 700">
                请输入文章标题
            </div>
<%--            <button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>--%>
        </div>
    </div>
</div>
<jsp:include page="./static/pagebottom.jsp"></jsp:include>
<script>

    $(".edit_button").click(function (){
        $(".max_write").css("display", "block");

        // 点击 login_div 以外的地方，自动关闭登录框
        $(document).on("mousedown", function (event) {
            var target = $(event.target);
            if (!target.closest(".max_write").length && !target.is(".max_write")) {
                $(".max_write").hide();
            }
        });
    });
    $(".delete_caidan").click(function(){
        $(".delete_xiugai").slideToggle("slow");
    });

    $('.button_submit').click(function (){
        var avatarImg = document.getElementById('previewImage2');
        const user_id2 = ${user_list.id}
        const name = $('.input_name').val()
        const tel_phone = $('input[name="tel_number"]').val()
        const password = $('.input_password').val()
        const articleImg = $("#base64TextArea").val().trim();
        var toastElList = [].slice.call(document.querySelectorAll('.toast'))
        var newpassword = password !== '' ? password : null;
        var toastList = toastElList.map(function(toastEl) {
            return new bootstrap.Toast(toastEl)
        })
        $("#base64TextArea").val("${user_list.photo}")
        $.ajax({
            url: "/update_userdata", // 获取表单的提交地址
            method: "POST",
            data: {
                id:user_id2 ,
                name: name ,
                password: newpassword,
                tel_phone:tel_phone,
                photo:articleImg
            },
            success: function(response) {
                console.log("--------------" + response.user)
                var user = response.user;
                console.log("------------------------------" + user)
                var user_data_name = document.querySelector('.user_data_name')
                user_data_name.innerHTML = user.name;
                console.log("-----------------------" + user_data_name); // 在控制台打印返回的数据
                $('.input_name').val(user.name)
                $('input[name="tel_number"]').val(user.phoneNumber)
                $("#base64TextArea").val(user.photo);
                avatarImg.src =  $("#base64TextArea").val(); // 修改为新的图片路径
                console.log(JSON.stringify(response));
                $(".toast-body").html("更改成功！");
                toastList.forEach(toast => toast.show())
            },
            error: function() {
                $(".toast-body").html("更改失败！");
                toastList.forEach(toast => toast.show())
            }
        });

    });

    function searchEssay(method) {
        const userId = ${user_list.id};
        console.log(userId)
        // 发送 AJAX 请求
        if (method == 1){
            $(".user_favorite").removeClass("active")
            $(".user_essay").removeClass("active")
            $(".user_like").addClass("active")
            $(".div_text").html("暂时没有喜欢文章记录")
        }else if (method == 2){
            $(".user_favorite").addClass("active")
            $(".user_essay").removeClass("active")
            $(".user_like").removeClass("active")
            $(".div_text").html("暂时没有收藏文章记录")
        }else if (method == 3){
            $(".user_favorite").removeClass("active")
            $(".user_essay").addClass("active")
            $(".user_like").removeClass("active")
            $(".div_text").html("暂时没有发布文章记录")

        }
        $.ajax({
            type: "GET",
            url: "/user_dongtai",
            data: {
                id: userId,
                method: method
            },
            success: function(response) {
                $("#essay_div").empty(); // 清空原有的文章列表
                if (response.essays != null && response.essays != ''){
                    console.log("---------------------" + response.essays)
                    var reso = response.essays
                    $(".not_data").css("display","none")
                    $(".not_data2").css("display","none")
                    // var essays = response.essays;
                    $.each(response.essays, function(index, essay) {
                        console.log(method + "Fangfa+++++++++++++++++++++++++++++")
                        var timestamp = essay.gmtCreate; // 假设这是时间戳
                        var date = new Date(timestamp);
                        var options = { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' };
                        var formattedTime = date.toLocaleString('ja-JP', options).replace(/\//g, '-');
                        var essay_data = '<div class="more_essay">';
                        essay_data += '<div class="essay_img">';
                        essay_data += '<c:if test="${not empty sessionScope.userid}">';
                        essay_data += '<a href="/essay_page?id=' + essay.id + '&user_id=' + ${sessionScope.userid} + '" target="_blank"><img class="essay_photo"  src="' + essay.articleImg + '" alt="冰川"></a>';
                        essay_data += '</c:if>';
                        essay_data += '<c:if test="${empty sessionScope.userid}">';
                        essay_data += '<a href="/essay_page?id=' + essay.id + '" target="_blank"><img class="essay_photo"  src="' + essay.articleImg + '" alt="冰川"></a>';
                        essay_data += '</c:if>';
                        essay_data += '</div>';
                        essay_data += '<div class="essay_content">';
                        essay_data += '<div class="essay_title">';
                        essay_data += '<c:if test="${not empty sessionScope.userid}">';
                        essay_data += '<a href="/essay_page?id=' + essay.id + '&user_id=' + ${sessionScope.userid} + '" target="_blank"><p>' + essay.articleTitle + '</p></a>';
                        essay_data += '</c:if>';
                        essay_data += '<c:if test="${empty sessionScope.userid}">';
                        essay_data += '<a href="/essay_page?id=' + essay.id + '" target="_blank"><p>' + essay.articleTitle + '</p></a>';
                        essay_data += '</c:if>';
                        essay_data += '</div>';
                        essay_data += '<div class="essay_text">';
                        essay_data += '<p>';
                        essay_data += essay.articleData;
                        essay_data += '</p>';
                        essay_data += '</div>';
                        essay_data += '<div class="essay_data">';
                        essay_data += '<div class="author_name">';
                        essay_data += '<p><a href="/userpage?id=' + essay.author + '">' + essay.authorName + '</a></p>';
                        essay_data += '</div>';
                        essay_data += '<div class="create_time">';
                        essay_data += '<p>' + formattedTime + '</p>';
                        essay_data += '</div>';
                        if (method == 3 && essay.author == ${sessionScope.userid}){
                            <%--console.log(<c:if test="${sessionScope.userid == essay.author}">)--%>
                            <%--essay_data += '<c:if test="${sessionScope.userid == essay.author}">'--%>
                            essay_data += '<div class="dropdown">'
                            essay_data += '<button type="button" class="dropbtn" onclick="delete_essay(' + essay.id + ')">';                            essay_data += '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">'
                            essay_data += '<path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>'
                            essay_data += '</svg>'
                            essay_data += '</button>'
                            essay_data += '<button class="dropbtn"  onclick="update_essay(' + essay.id + ')">';                            essay_data += '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'
                            essay_data += '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'
                            essay_data += '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'
                            essay_data += '</svg>'
                            essay_data += '</button>'
                            essay_data += '</div>'
                            <%--essay_data += '</c:if>'--%>
                        }
                        essay_data += '</div>';
                        essay_data += '</div>';
                        essay_data += '<div class="essay_phone_title">';
                        essay_data += '<a href="/essay_page?id=' + essay.id + '" target="_blank"><p>' + essay.articleTitle + '</p></a>';
                        essay_data += '</div>';
                        essay_data += '</div>';
                        $("#essay_div").append(essay_data);
                    });
                }else{
                    $(".not_data").css("display","block")
                    $(".not_data2").css("display","block")
                }
                // 处理返回的数据，例如更新页面内容
                console.log(response); // 在控制台输出返回的数据
                // 这里根据返回的数据更新页面显示的内容
            },
            error: function() {
                // 发生错误时的处理
                console.log("发生错误");
            }
        });
    }

    //点击删除按钮
    function delete_essay(id){
        console.log("文章的id===" + id)
        $.ajax({
            type: "GET",
            url: "/delete_essay",
            data: {
                id: id
            },
            success : function(response) {
                var toastElList = [].slice.call(document.querySelectorAll('.toast'))
                var toastList = toastElList.map(function(toastEl) {
                    return new bootstrap.Toast(toastEl)
                })
                $(".toast-body").html("文章删除成功！");
                toastList.forEach(toast => toast.show())
            },
            error: function() {
                console.log("发生错误");
                $(".toast-body").html("文章删除失败！");
                toastList.forEach(toast => toast.show())
            }
        });
    }

    //修改文章
    function update_essay(id){
        window.location.href = "/update_essay?id=" + id;
    }

    //这就是
    $(".more_essay").mouseenter(function(){
        // $(this).find(".essay_img").css("width","285px")
        $(this).find(".essay_photo").css("transform" , "scale(1.08)");
        $(this).find(".essay_content").css("margin-left", "2px");
        // $(this).find(".more_essay").css("width","180px")

    });

    $(".more_essay").mouseleave(function(){
        // $(this).find(".essay_img").css("width","300px")
        $(this).find(".essay_photo").css("transform" , "scale(1)");
        $(this).find(".essay_content").css("margin-left", "12px");
        // $(this).find(".more_essay").css("width","193px")

    });


    //上传图片
    //上传头部照片
    function handleFile() {
        const fileInput = document.getElementById("fileInput");
        const file = fileInput.files[0];
        const reader = new FileReader();
        // const previewImage = document.querySelector(".previewImage");
        // const privewImage2 = document.querySelector(".previewImage2");
        // const base64TextArea = document.querySelector(".base64TextArea");
        var toastElList = [].slice.call(document.querySelectorAll('.toast'))
        var toastList = toastElList.map(function(toastEl) {
            return new bootstrap.Toast(toastEl)
        })
        // 判断文件大小是否超过600KB
        if (file.size > 1 * 1024 * 1024) {
            $(".toast-body").html("图片大小不能超过1MB");
            toastList.forEach(toast => toast.show())
            return;
        }

        reader.onload = function(event) {
            const base64String = event.target.result;
            previewImage.src = base64String;
            base64TextArea.value = base64String;
            $(".toast-body").html("上传图片成功");
            toastList.forEach(toast => toast.show())
        };

        reader.readAsDataURL(file);
    }
    document.getElementById('fileInput').addEventListener('change', handleFile);

    //用户操作

</script>
</body>
<script src="/js/essay.js"></script>
</html>