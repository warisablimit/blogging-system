
<%@ page isELIgnored="false" %>
<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/12/4
  Time: 17:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>文章|${essay.articleTitle}</title>
    <link rel="stylesheet" href="/css/essay.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
<jsp:include page="./static/pagetop.jsp"></jsp:include>
<div class="page_center" style="background-color: #ffffff !important;">
    <div class="user_essay_id">
        <p class="essay_id">${essay.id}</p>
        <p class="user_id">${sessionScope.userid}</p>
        <p class="user_name">${sessionScope.username}</p>
        <p class="is_like">${panduan}</p>
        <p class="is_favorite">${panduan2}</p>
        <%--                <p class="is_like">1</p>--%>
    </div>
    <div class="author_div">
        <div class="user_image">

            <a href="/userpage?id=${essay.author}"><img src="${essay.authorPhoto}" alt="${author}的头像"></a>
        </div>
        <div class="user_name">
            <a href="/userpage?id=${essay.author}"><p>${essay.authorName}</p></a>
        </div>
    </div>
    <div class="essay_max">
        <div class="essay_top_img">
            <img src="${essay.articleImg}" alt="图片">
        </div>
        <div class="essay_title">
            <p>${essay.articleTitle}</p>
        </div>
        <div class="phone_author_div">
            <div class="user_image">
                <a href="/userpage?id=${essay.author}"><img src="${essay.authorPhoto}" alt="${essay.authorName}的头像"></a>
            </div>
            <div class="user_name">
                <a href="/userpage?id=${essay.author}">${essay.authorName}</a>
            </div>
        </div>
        <div class="essay_data">
            ${essay.articleContent}
        </div>
    </div>
    <div class="line_div"></div>
    <div class="user_caozuo">
        <div class="user_like">
            <c:if test="${not empty sessionScope.userid}">
                <a>
                    <button onclick="likeEssay(${essay.id}, ${sessionScope.userid},1)" class="like_button"></button>
                </a>
            </c:if>
            <c:if test="${empty sessionScope.userid}">
                <button  class="like_button"></button>
            </c:if>
        </div>
        <p id="essayLikeCount">${essay.like}</p>
        <div class="user_collection">
            <c:if test="${not empty sessionScope.userid}">
                <a class="favorite_a">
                    <button onclick="likeEssay(${essay.id}, ${sessionScope.userid},0)"  class="collection_button"></button>
                </a>
            </c:if>
            <c:if test="${empty sessionScope.userid}">
                <button  class="collection_button"></button>
            </c:if>
        </div>
        <p id="essayfavoriteIdCount">${essay.favoriteId}</p>
        <div class="user_pinglun">
            <button class="pinglun_button"></button>
        </div>
        <p id="comments_num">${essay.comments}</p>
    </div>





    <div class="tui_jian">
        <div class="tuijian_essay">
            <div class="tuijian_essay_title">
                <a href="#"><p>刚刚过去的六月底，一条</p></a>
            </div>
            <div class="tuijian_essay_text">
                <p>刚刚过去的六月底，一条刚刚过去的六月底，一条刚刚过去的六月底，一条</p>
            </div>
        </div>
    </div>










    <div class="comments_div">
        <c:forEach items="${comments}" var="comment">
            <div class="one_comments">
                <div class="comments_user">
                    <div class="user_touxiang">
                        <a href="/userpage?id=${comment.userId}"><img src="${comment.authorPhoto}" alt="${comment.userId}的头像"></a>
                    </div>
                    <div class="comment_user"><a href="/userpage?id=${comment.userId}">${comment.authorName}</a></div>
                </div>
                <div class="comment_data">
                    <div class="comments_content">
                        ${comment.content}
                    </div>
                    <div class="comment_time">
                        ${comment.createdAt}
                    </div>
                </div>
            </div>
        </c:forEach>
        <div class="comment_input">
            <form id="commentForm">
                <div class="comment_text">
                    <textarea class="comment_textarea" name="content" placeholder="请输入评论" rows="2" maxlength="100"></textarea>
                </div>
                <div class="comment_button">
                    <c:if test="${not empty sessionScope.userid}">
                        <input onclick="addComment()" type="button" value="发布">
                    </c:if>
                </div>
            </form>
        </div>
    </div>
    <div class="toc" >
        <ul></ul>
    </div>

</div>


<script>

    if (window.innerWidth < 606) {
        // 页面宽度小于606px的处理逻辑
        $(".line_div").click(function () {
            $(".user_caozuo").css("display","block")
            $(document).on("mousedown", function (event) {
                var target = $(event.target);
                if (!target.closest(".user_caozuo").length && !target.is(".user_caozuo")) {
                    $(".user_caozuo").hide();
                }
            });
        })
    } else {
        // 页面宽度大于等于606px的处理逻辑
        console.log("页面宽度大于等于606px");
    }



    //            <button onclick="scrollToTopHeight()" class="collection_button"></button>
</script>



<jsp:include page="./static/pagebottom.jsp"></jsp:include>
<script type="text/javascript" src="/js/essay.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</body>
</html>