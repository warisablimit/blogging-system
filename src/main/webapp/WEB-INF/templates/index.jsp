<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/7/5
  Time: 17:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>首页</title>
    <link href="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="/css/color-thief-2.4.0/src/color-thief.js"></script>
</head>
<body>

<jsp:include page="./static/pagetop.jsp"></jsp:include>
<div class="page_center">
    <div class="essay_div">
        <!-- 轮番图的div -->
        <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <c:forEach items="${up_essays}" var="essay" varStatus="status">
                    <c:choose>
                        <c:when test="${status.index eq 0}">
                            <div class="carousel-item active" data-bs-interval="3000">
                                <c:if test="${not empty sessionScope.userid}">
                                    <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_self">
                                        <img src="${essay.articleImg}" class="d-block w-100" alt="${essay.articleTitle}">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5>${essay.articleTitle}</h5>
                                        </div>
                                    </a>
                                </c:if>
                                <c:if test="${empty sessionScope.userid}">
                                    <a href="/essay_page?id=${essay.id}" target="_self">
                                        <img src="${essay.articleImg}" class="d-block w-100" alt="${essay.articleTitle}">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5>${essay.articleTitle}</h5>
                                        </div>
                                    </a>
                                </c:if>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="carousel-item" data-bs-interval="3000">
                                <c:if test="${not empty sessionScope.userid}">
                                    <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_self">
                                        <img src="${essay.articleImg}" class="d-block w-100" alt="${essay.articleTitle}">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5>${essay.articleTitle}</h5>
                                        </div>
                                    </a>
                                </c:if>
                                <c:if test="${empty sessionScope.userid}">
                                    <a href="/essay_page?id=${essay.id}" target="_self">
                                        <img src="${essay.articleImg}" class="d-block w-100" alt="${essay.articleTitle}">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5>${essay.articleTitle}</h5>
                                        </div>
                                    </a>
                                </c:if>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
        <div class="tuijian_div">
            <p>
                文章推荐
            </p>
        </div>

        <div id="essay_div">
            <c:forEach items="${essays}" var="essay">
                <div class="more_essay">
                    <div class="essay_img">
                        <c:if test="${not empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_blank"><img class="essay_photo"  src="${essay.articleImg}" alt="冰川"></a>
                        </c:if>
                        <c:if test="${empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}" target="_blank"><img class="essay_photo"  src="${essay.articleImg}" alt="冰川"></a>
                        </c:if>
                    </div>
                    <div class="essay_content">
                        <div class="essay_title">
                            <c:if test="${not empty sessionScope.userid}">
                                <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_blank"><p>${essay.articleTitle}</p></a>
                            </c:if>
                            <c:if test="${empty sessionScope.userid}">
                                <a href="/essay_page?id=${essay.id}" target="_blank"><p>${essay.articleTitle}</p></a>
                            </c:if>
                        </div>
                        <div class="essay_text">
                            <p>
                                    ${essay.articleData}
                            </p>
                        </div>
                        <div class="essay_data">
                            <div class="author_name">
                                <p><a href="/userpage?id=${essay.author}">${essay.authorName}</a></p>
                            </div>
                            <div class="create_time">
                                <p>${essay.gmtCreate}</p>
                            </div>
<%--                            <div class="dropdown-center">--%>
<%--                                <button class="btn-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">--%>
<%--                                </button>--%>
<%--                                <ul class="dropdown-menu">--%>
<%--                                    <li><a class="dropdown-item" href="#">修改</a></li>--%>
<%--                                    <li><a class="dropdown-item" href="#">删除</a></li>--%>
<%--                                </ul>--%>
<%--                            </div>--%>

                        </div>
                    </div>
                    <div class="essay_phone_title">
                        <c:if test="${not empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}&user_id=${sessionScope.userid}" target="_self"><p>${essay.articleTitle}</p></a>
                        </c:if>
                        <c:if test="${empty sessionScope.userid}">
                            <a href="/essay_page?id=${essay.id}" target="_self"><p>${essay.articleTitle}</p></a>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<jsp:include page="./static/pagebottom.jsp"></jsp:include>
<script>
    //页面跳转
    function handleClick() {
        var essayId = "${essay.id}";  // 获取 essay.id 的值
        var userId = "${sessionScope.userid}";  // 获取 sessionScope.userid 的值

        // 向服务器发送 AJAX 请求
        $.ajax({
            url: "essay_page/",
            type: "POST",
            data: {
                id: essayId,
                user_id: userId
            },
            success: function(response) {
                // 请求成功后的处理逻辑
            },
            error: function(error) {
                // 请求失败后的处理逻辑
            }
        });
    }

    $(".more_essay").mouseenter(function(){
        // $(this).find(".essay_img").css("width","285px")
        $(this).find(".essay_photo").css("transform" , "scale(1.08)");
        $(this).find(".essay_content").css("margin-left", "2px");
        // $(this).find(".more_essay").css("width","180px")

    });

    $(".more_essay").mouseleave(function(){
        // $(this).find(".essay_img").css("width","300px")
        $(this).find(".essay_photo").css("transform" , "scale(1)");
        $(this).find(".essay_content").css("margin-left", "12px");
        // $(this).find(".more_essay").css("width","193px")

    });

    //根据图片的颜色来改变文字的颜色
    // 获取轮播项目的背景图片和标题元素
    // var carouselItems = document.querySelectorAll('.carousel-item');
    // var captionTitles = document.querySelectorAll('.carousel-caption h5');
    //
    // // 遍历每个轮播项目
    // carouselItems.forEach(function(item, index){
    //     // 创建一个新的Image对象
    //     var img = new Image();
    //     img.src = item.querySelector('img').getAttribute('src');
    //     img.onload = function() {
    //         // 计算背景图片的平均颜色
    //         var colorThief = new ColorThief();
    //         var rgb = colorThief.getColor(img);
    //         var brightness = (299 * rgb[0] + 587 * rgb[1] + 114 * rgb[2]) / 1000;
    //
    //         // 根据背景图的亮度判断设置标题颜色为白色或黑色
    //         if (brightness > 125) {
    //             captionTitles[index].style.color = 'black';
    //         } else {
    //             captionTitles[index].style.color = 'white';
    //         }
    //     };
    // });

</script>
</body>
<script src="/js/index.js"></script>
<script src="/js/essay.js"></script>
<!-- <script>
    window.location.href = "text.html"
</script> -->
</html>