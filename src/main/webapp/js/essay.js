$(document).ready(function(){
    $(".more_essay").mouseenter(function(){
        // $(this).find(".essay_img").css("width","285px")
        $(this).find(".essay_photo").css("transform" , "scale(1.08)");
        $(this).find(".essay_content").css("margin-left", "2px");
        // $(this).find(".more_essay").css("width","180px")

    });

    $(".more_essay").mouseleave(function(){
        // $(this).find(".essay_img").css("width","300px")
        $(this).find(".essay_photo").css("transform" , "scale(1)");
        $(this).find(".essay_content").css("margin-left", "12px");
        // $(this).find(".more_essay").css("width","193px")

    });

    $(".comment_textarea").focus(function () {
        $(".comment_input").css("height","120px")
        $(".comment_textarea").css("height","110px")
        setTimeout(function (){
        },500)
    })
    $(".comment_textarea").blur(function () {
        $(".comment_input").css("height","50px")
        $(".comment_textarea").css("height","40px")
    })

    if ($(".is_favorite").html() == 'true'){
        $(".collection_button").addClass("collection_active")
        // 通过id选择器获取<a>元素并更改href属性
    }else{
        $(".collection_button").removeClass("collection_active")
    }

    if ($(".is_like").html() == 'true'){
        $(".like_button").addClass("active")
        // 通过id选择器获取<a>元素并更改href属性
    }else{
        $(".like_button").removeClass("active")
    }

    // $(".like_button").click(function (){
    //     $(".like_button").toggleClass("active")
    // })
    // $(".collection_button").click(function () {
    //     $(".collection_button").toggleClass("collection_active")
    // })
    $(".pinglun_button").click(function () {
        $(".pinglun_button").toggleClass("pinglun_active")
        $(".comments_div").css({
            "padding":"10px",
            "width":"500px"
        })
        // 点击 login_div 以外的地方，自动关闭登录框
        $(document).on("mousedown", function (event) {
            $(".pinglun_button").removeClass("pinglun_active")
            var target = $(event.target);
            if (!target.closest(".comments_div").length && !target.is(".comments_div")) {
                $(".comments_div").css({
                    "padding":"0",
                    "width":"0"
                });
            }
        });
    })

    //这里就是生成标题
    // 遍历所有的标题标签并生成目录
    $("h1, h2, h3, h4, h5, h6").each(function() {
        var tagName = $(this)[0].tagName.toLowerCase();
        var titleText = $(this).text();
        var titleLevel = parseInt(tagName.substring(1));

        if (!$(this).attr("id")) {
            $(this).attr("id", "toc-" + $(this).text().replace(/[^A-Za-z0-9]/g, "-"));
        }

        var tocItem = $("<li></li>");
        var tocLink = $("<a></a>").text(titleText).attr("href", "#" + $(this).attr("id"));
        tocLink.addClass("toc-level-" + titleLevel);
        tocItem.append(tocLink);
        $(".toc ul").append(tocItem);

        // 目录项链接的点击事件
        tocLink.on("click", function(e) {
            e.preventDefault();

            var target = $(this).attr("href");
            var navHeight = $(".page_top").height(); // 导航栏的高度

            $("html, body").animate({
                scrollTop: $(target).offset().top - navHeight
            }, 800);
        });
    });

})

//用户点击点赞按钮


function likeEssay(essayId, userId,if_function) {
    $(document).ready(function (){
        $.ajax({
            url: "/like",
            method: "GET",
            data: { id: essayId,
                user_id: userId ,
                if_data:if_function == 1 ? ($(".is_like").html() != 'true' ? 1 : 2) : ($(".is_favorite").html() != 'true' ? 3 : 4)
            },
            success: function(response) {
                //  if_function == 1 ? (if_data: $(".is_like").html() != 'true' ? 1 : 2):(if_data: $(".is_favorite").html() != 'true' ? 3 : 4)
                var essay = response.essay;
                // 更新页面中的内容和显示喜欢状态
                // 更新页面中的内容和显示喜欢状态
                if (if_function == 1){
                    var like = response.like;
                    if (essay && like !== undefined) {
                        var likeCountElement = document.getElementById("essayLikeCount");
                        var if_likedata = document.querySelector(".is_like");
                        if (likeCountElement) {
                            likeCountElement.innerText = essay.like;
                            if_likedata.innerHTML = like;
                            if ($(".is_like").html() == 'true'){
                                $(".like_button").addClass("active")
                            }else{
                                $(".like_button").removeClass("active")
                            }
                        }
                    }
                }
                //更新用户收藏的状态
                else if (if_function == 0){
                    console.log("-------------favorite")
                    var interaction = response.interaction;
                    if (essay && interaction !== undefined) {
                        var essayfavoriteIdCount = document.getElementById("essayfavoriteIdCount");
                        var is_favorite = document.querySelector(".is_favorite");
                        if (essayfavoriteIdCount) {
                            essayfavoriteIdCount.innerText = essay.favoriteId;
                            is_favorite.innerHTML = interaction;
                            if ($(".is_favorite").html() == 'true'){
                                $(".collection_button").addClass("collection_active")
                            }else{
                                $(".collection_button").removeClass("collection_active")
                            }
                        }
                    }
                }
                // 执行其他页面更新操作
            },
            error: function(error) {
                console.log(error);
            }
        });
    })
}

//添加评论
function addComment() {
        var content = $('.comment_textarea').val();
        var userId =$(".user_id").html();
        var userName = $(".user_name").html();
        var essayId = $(".essay_id").html();
        console.log("content -----------------" + content)
        console.log("userid -----------------" + userId)
        console.log("username -----------------" + userName)
        console.log("essayid -----------------" + essayId)
        if (content != "" && content != null){
            $.ajax({
                url: '/addComment',
                type: 'POST',
                data: {
                    content: content,
                    userId: userId,
                    userName: userName,
                    essayId: essayId
                },
                success: function(response) {
                    var comment = response.comment;
                    var timestamp = comment.createdAt; // 假设这是时间戳
                    var date = new Date(timestamp);
                    var options = { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' };
                    var formattedTime = date.toLocaleString('ja-JP', options).replace(/\//g, '-');
                    console.log(formattedTime); // 输出格式化后的时间 "2023-12-07 21:45:39"
                    console.log(formattedTime); // 输出格式化后的时间 "2023-12-07 21:45:39"
                    var commentHtml = `
                <div class="one_comments">
                    <div class="comments_user">
                        <div class="user_touxiang">
                            <a href="/user_data?method=user_essay&user_id=${comment.userId}"><img src="${comment.authorPhoto}" alt="${comment.userId}的头像"></a>
                        </div>
                        <div class="comment_user"><a href="/user_data?method=user_essay&user_id=${comment.userId}">${comment.authorName}</a></div>
                    </div>
                    <div class="comment_data">
                        <div class="comments_content">
                            ${comment.content}
                        </div>
                        <div class="comment_time">
                            ${formattedTime}
                        </div>
                    </div>
                </div>`;
                    console.log("评论内容--------------------------" , commentHtml)
                    $('.comments_div').prepend(commentHtml); // 在头部追加新的评论

                    $('.comment_textarea').val(''); // 清空评论输入框内容

                    //更新评论数
                    var essaydata = response.essay;
                    var comments_num = document.getElementById("comments_num");
                    comments_num.innerText = essaydata.comments;
                    //更新评论数
                },
                error: function() {
                    console.log('请求失败');
                    // 处理请求失败的逻辑
                }
            });
        }
}






// function addComment() {
//     var content = $('.comment_textarea').val();
//     var userId =$(".user_id").html();
//     var userName = $(".user_name").html();
//     var essayId = $(".essay_id").html();
//     console.log("content -----------------" + content)
//     console.log("userid -----------------" + userId)
//     console.log("username -----------------" + userName)
//     console.log("essayid -----------------" + essayId)
//
//     $.ajax({
//         url: '/addComment',
//         type: 'POST',
//         data: {
//             content: content,
//             userId: userId,
//             userName: userName,
//             essayId: essayId
//         },
//         success: function(response) {
//             $('.comments').empty(); // 清空原有的评论
//             $.each(response.comments, function(index, comment) {
//                 var commentHtml = `
//                     <div class="one_comments">
//                         <div class="comments_user">
//                             <div class="user_touxiang">
//                                 <a href="/user_data?method=user_essay&user_id=${comment.userId}"><img src="./css/image/fish_ai.jp" alt="${comment.userId}的头像"></a>
//                             </div>
//                             <div class="comment_user"><a href="/user_data?method=user_essay&user_id=${comment.userId}">${comment.authorName}</a></div>
//                         </div>
//                         <div class="comment_data">
//                             <div class="comments_content">
//                                 ${comment.content}
//                             </div>
//                             <div class="comment_time">
//                                 ${comment.createdAt}
//                             </div>
//                         </div>
//                     </div>`;
//                 console.log("评论内容--------------------------" , commentHtml)
//                 $('.comments_div').prepend(commentHtml); // 在头部追加新的评论
//                 $('.comment_textarea').val(''); // 清空评论输入框内容
//                 //更新评论数
//                 var essaydata = response.essay;
//                 var comments_num = document.getElementById("comments_num");
//                 comments_num.innerText = essaydata.comments;
//                 //更新评论数
//             });
//         },
//         error: function() {
//             console.log('请求失败');
//             // 处理请求失败的逻辑
//         }
//     });
// }






