//package com.atguigu.ssm.service.impl;
//
//import com.atguigu.ssm.mapper.MinFeedBackMapper;
//import com.atguigu.ssm.pojo.MinFeedBack;
//import com.atguigu.ssm.service.MinFeedBackService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Service
//@Transactional
//public class MinFeedBackServieImpl implements MinFeedBackService {
//
//    @Autowired
//    private MinFeedBackMapper minFeedBackMapper;
//
//    @Override
//    public List<MinFeedBack> MinFeedBackList() {
//        return minFeedBackMapper.MinFeedBackList();
//    }
//
//    @Override
//    public int InsertMinFeedBack(String content, int author, String author_name, int ffeedback, String huifuobject) {
//        return minFeedBackMapper.InserMinFeedBackList(content,author,author_name,ffeedback,huifuobject);
//    }
//
//    @Override
//    public int deleteMinFeedBack(int id) {
//        return  minFeedBackMapper.deleteMinFeedBack(id);
//    }
//
//    @Override
//    public int deleteMinFeedBacks(int feedback_id) {
//        return minFeedBackMapper.deleteMinFeedBacks(feedback_id);
//    }
//}
