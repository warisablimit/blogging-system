package com.atguigu.ssm.service.impl;

import com.atguigu.ssm.mapper.InteractionMapper;
import com.atguigu.ssm.pojo.Essay;
import com.atguigu.ssm.pojo.User_interaction;
import com.atguigu.ssm.service.InteractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InteractionServiceImpl implements InteractionService {

    @Autowired
    private InteractionMapper interactionMapper;

    /**
     * 获取指定用户的所有交互记录
     * @param id 用户ID
     * @return 交互记录列表
     */
    @Override
    public List<User_interaction> listInteractions(int id) {
        return interactionMapper.list(id);
    }

    /**
     * 获取指定用户喜欢的所有文章
     * @param userId 用户ID
     * @return 文章列表
     */
    @Override
    public List<User_interaction> listLikedEssays(int userId) {
        return interactionMapper.likeEssayList(userId);
    }

    /**
     * 获取指定用户互动过的所有文章
     * @param userId 用户ID
     * @return 文章列表
     */
    @Override
    public List<User_interaction> listInteractedEssays(int userId) {
        return interactionMapper.interactionEssayList(userId);
    }

    /**
     * 添加用户与文章的交互记录
     * @param userId 用户ID
     * @param favoriteId 文章ID
     */
    @Override
    public void addInteraction(int userId, int favoriteId) {
        interactionMapper.addInteraction(userId, favoriteId);
    }

    /**
     * 删除用户与文章的交互记录
     * @param userId 用户ID
     * @param favoriteId 文章ID
     */
    @Override
    public void deleteInteraction(int userId, int favoriteId) {
        interactionMapper.deleteInteraction(userId, favoriteId);
    }

    /**
     * 添加用户对文章的喜欢记录
     * @param userId 用户ID
     * @param likeId 文章ID
     */
    @Override
    public void addLike(int userId, int likeId) {
        interactionMapper.addLike(userId, likeId);
    }

    /**
     * 删除用户对文章的喜欢记录
     * @param userId 用户ID
     * @param id 文章ID
     */
    @Override
    public void deleteLike(int userId, int id) {
        interactionMapper.deleteLike(userId, id);
    }

    /**
     * 判断用户是否喜欢指定文章
     * @param userId 用户ID
     * @param id 文章ID
     * @return 布尔值，表示用户是否喜欢指定文章
     */
    @Override
    public boolean hasLiked(int userId, int id) {
        int count = interactionMapper.likeSelect(userId, id);
        return count > 0;
    }

    /**
     * 判断用户是否互动过指定文章
     * @param userId 用户ID
     * @param id 文章ID
     * @return 布尔值，表示用户是否互动过指定文章
     */
    @Override
    public boolean hasInteracted(int userId, int id) {
        int count = interactionMapper.interactionSelect(userId, id);
        return count > 0;
    }

    @Override
    public void deleteInteractionByEssayId(int essayId) {
        interactionMapper.deleteInteractionByEssayId(essayId);
    }
}