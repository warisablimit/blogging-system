package com.atguigu.ssm.service.impl;

import com.atguigu.ssm.mapper.UserMapper;
import com.atguigu.ssm.pojo.Blog_User;
import com.atguigu.ssm.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<Blog_User> listUsers() {
        return userMapper.listUsers();
    }

    @Override
    public Blog_User getUser(int id) {
        return userMapper.getUser(id);
    }

    @Override
    public Blog_User getUserData(int id) {
        return userMapper.getUserData(id);
    }

    @Override
    public String getUserName(int id) {
        return userMapper.getUserName(id);
    }

    @Override
    public Blog_User getOneUser(String name, String password) {
        return userMapper.getOneUser(name,password);
    }

    @Override
    public void deleteUser(int id) {
        userMapper.deleteUser(id);
    }

    @Override
    public List<Blog_User> searchUsers(String name) {
        return userMapper.searchUsers(name);
    }

    @Override
    public int insertUser(String name,String password,String photo) {
        return userMapper.insertUser(name,password,photo);
    }

    @Override
    public void updateUser(@Param("name") String name, @Param("password")String password, @Param("id") int id,
                           @Param("phoneNumber")String phoneNumber,
                           @Param("photo")String photo,@Param("testFlag") int testFlag) {
        userMapper.updateUser(name,password,id,phoneNumber,photo,testFlag);
    }
}
