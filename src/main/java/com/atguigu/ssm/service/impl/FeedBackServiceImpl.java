//package com.atguigu.ssm.service.impl;
//
//
//import com.atguigu.ssm.mapper.FeedBackMapper;
//import com.atguigu.ssm.pojo.FeedBack;
//import com.atguigu.ssm.service.FeedBackService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Service
//@Transactional
//public class FeedBackServiceImpl implements FeedBackService {
//
//
//    @Autowired
//    private FeedBackMapper feedBackMapper;
//
//    @Override
//    public List<FeedBack> FeedBackList() {
//        return feedBackMapper.FeedBackList();
//    }
//
//    @Override
//    public int insertFeedBack(String content,int id,String author) {
//        return feedBackMapper.insertFeedBack(content,id,author);
//    }
//
//    @Override
//    public int deleteFeedBack(int id){
//        return feedBackMapper.deleteFeedBack(id);
//    }
//}
