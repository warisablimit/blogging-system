package com.atguigu.ssm.service.impl;

import com.atguigu.ssm.mapper.EssayMapper;
import com.atguigu.ssm.pojo.Essay;
import com.atguigu.ssm.service.EssayService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EssayServiceImpl implements EssayService {

    @Autowired
    private EssayMapper essayMapper;

    /**
     * 获取所有文章列表
     *
     * @return 文章列表
     */
    @Override
    public List<Essay> getAllEssayList() {
        return essayMapper.AllEssayList();
    }

    /**
     * 获取按点赞、收藏、评论降序排列的文章列表
     *
     * @return 文章列表
     */
    @Override
    public List<Essay> getLikeFavoriteCommentEssayList() {
        return essayMapper.likeFavoriteCommentEssayList();
    }

    /**
     * 通过文章ID获取文章详细信息
     *
     * @param id 文章ID
     * @return 文章对象
     */
    @Override
    public Essay getEssayById(int id) {
        return essayMapper.getEssayById(id);
    }

    /**
     * 获取某个用户发布的所有文章列表
     *
     * @param userId 用户ID
     * @return 文章列表
     */
    @Override
    public List<Essay> getUserEssayList(int userId) {
        return essayMapper.userEssayList(userId);
    }

    /**
     * 添加文章
     *
     * @param
     */

    @Override
    public void addEssay(@Param("author") int author, @Param("authorName") String authorName, @Param("articleTitle") String articleTitle, @Param("articleImg") String articleImg, @Param("articleData") String articleData,
                         @Param("articleContent") String articleContent,
                         @Param("authorPhoto") String authorPhoto) {
        essayMapper.insertEssay(author,authorName,articleTitle,articleImg,articleData,articleContent,authorPhoto);
    }

    /**
     * 修改文章
     *
     * @param
     */
    @Override
    public void updateEssay(@Param("articleTitle") String articleTitle,@Param("articleImg") String articleImg,@Param("articleData") String articleData,@Param("articleContent") String articleContent,@Param("id") int id) {
        essayMapper.updateEssay(articleTitle,articleImg,articleData,articleContent,id);
    }

    /**
     * 删除文章
     *
     * @param id 文章ID
     */
    @Override
    public void deleteEssay(int id) {
        essayMapper.deleteEssay(id);
    }

    /**
     * 通过关键词模糊搜索文章
     *
     * @param keyword 搜索关键词
     * @return 文章列表
     */
    @Override
    public List<Essay> searchEssay(String keyword) {
        return essayMapper.searchEssay(keyword);
    }

    @Override
    public void decrementFavoriteCount(Map<String, Object> params) {
        essayMapper.decrementFavoriteCount(params);
    }

    @Override
    public List<Essay> likeEssayList(List<Integer> likeIdList) {
        return essayMapper.likeEssayList(likeIdList);
    }

    @Override
    public List<Essay> InteractedEssayList(List<Integer> likeIdList) {
        return essayMapper.InteractedEssayList(likeIdList);
    }


    //查询点赞和收藏数最高的文章
    @Override
    public List<Essay> sexupEssayList() {
        return essayMapper.sexupEssayList();
    }

    @Override
    public void updateAuthorNameByUserId(int userId, String authorName) {
        essayMapper.updateAuthorNameByUserId(userId,authorName);
    }


}