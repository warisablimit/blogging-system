package com.atguigu.ssm.service.impl;

import com.atguigu.ssm.mapper.CommentMapper;
import com.atguigu.ssm.pojo.Comment;
import com.atguigu.ssm.service.CommentService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Override
    public List<Comment> listComment() {
        return commentMapper.listComment();
    }

    @Override
    public List<Comment> listCommentByEssay(int essay) {
        return commentMapper.listCommentByEssay(essay);
    }

    @Override
    public List<Comment> listCommentByUser(int user) {
        return commentMapper.listCommentByUser(user);
    }

    @Override
    public void addComment(@Param("userId") int userId, @Param("essayId") int essayId, @Param("content") String content, @Param("authorName") String authorName,@Param("athorPhoto") String authorPhoto) {
        commentMapper.addComment(userId,essayId,content,authorName,authorPhoto);
    }

    @Override
    public Comment getComment(int userId, int essayId, String content, String authorName) {
        return commentMapper.getComment(userId,essayId,content,authorName);
    }

    //查询某个评论


    @Override
    public void deleteComment(int id) {
        commentMapper.deleteComment(id);
    }

    @Override
    public void deleteCommentByUserAndEssay(int userId, int essayId) {

    }

    @Override
    public List<Comment> searchCommentByContent(String text) {
        return commentMapper.searchCommentByContent(text);
    }



    /**
     * 删除指定文章ID的方法
     *
     * @param essayId 要删除的文章ID
     */
    @Override
    public void essay_id_delete(int essayId) {
        commentMapper.essay_id_delete(essayId);
    }

    @Override
    public void updateAuthorNameByUserId(int userId, String authorName) {
        commentMapper.updateAuthorNameByUserId(userId,authorName);
    }


    //


}
