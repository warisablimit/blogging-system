package com.atguigu.ssm.service;

import com.atguigu.ssm.pojo.Essay;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EssayService {

    /**
     * 获取所有文章列表
     *
     * @return 文章列表
     */
    List<Essay> getAllEssayList();

    /**
     * 获取按点赞、收藏、评论降序排列的文章列表
     *
     * @return 文章列表
     */
    List<Essay> getLikeFavoriteCommentEssayList();

    /**
     * 通过文章ID获取文章详细信息
     *
     * @param id 文章ID
     * @return 文章对象
     */
    Essay getEssayById(int id);

    /**
     * 获取某个用户发布的所有文章列表
     *
     * @param userId 用户ID
     * @return 文章列表
     */
    List<Essay> getUserEssayList(int userId);

    /**
     * 添加文章
     *
     * 文章对象
     */
    void addEssay(@Param("author") int author, @Param("authorName") String authorName,
                  @Param("articleTitle") String articleTitle, @Param("articleImg") String articleImg,
                  @Param("articleData") String articleData, @Param("articleContent") String articleContent,
                  @Param("authorPhoto") String authorPhoto);

    /**
     * 修改文章
     *
     * @param
     */
    void updateEssay(@Param("articleTitle") String articleTitle,@Param("articleImg") String articleImg,

                     @Param("articleData") String articleData,@Param("articleContent") String articleContent,@Param("id") int id);

    /**
     * 删除文章
     *
     * @param id 文章ID
     */
    void deleteEssay(int id);

    /**
     * 通过关键词模糊搜索文章
     *
     * @param keyword 搜索关键词
     * @return 文章列表
     */
    List<Essay> searchEssay(String keyword);

    //用户操作
    void decrementFavoriteCount(Map<String, Object> params);


    //查询用户收藏的文章
    List<Essay> likeEssayList(List<Integer> likeIdList);


    //查询用户收藏的文章
    List<Essay> InteractedEssayList(List<Integer> likeIdList);


    //查询点赞和收藏数为最高的文章
    List<Essay> sexupEssayList();


    void updateAuthorNameByUserId(@Param("userId") int userId,@Param("authorName") String authorName);

}