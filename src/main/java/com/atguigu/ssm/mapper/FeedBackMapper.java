//package com.atguigu.ssm.mapper;
//
//import com.atguigu.ssm.pojo.FeedBack;
//import org.apache.ibatis.annotations.*;
//
//import java.util.List;
//
//public interface FeedBackMapper {
//
////    @Results(id = "feedbackMap", value = {
////            @Result(property = "id", column = "id"),
////            @Result(property = "content", column = "content"),
////            @Result(property = "gmtCreate", column = "gmt_create"),
////            @Result(property = "author", column = "author"),
////            @Result(property = "authorName", column = "author_name")
////    })
//    @Select("SELECT * FROM feedback")
//    List<FeedBack> FeedBackList();
//
////    @Insert("INSERT INTO feedback (content, author) VALUES (#{content}, #{author})")
//
//    int insertFeedBack(@Param("content") String content,@Param("author") int author_id, @Param("author_name") String author_name);
//
//    int deleteFeedBack(@Param("id") int id);
//}
