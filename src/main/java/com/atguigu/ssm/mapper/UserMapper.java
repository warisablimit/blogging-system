package com.atguigu.ssm.mapper;

import com.atguigu.ssm.pojo.Blog_User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {

    /**
     * 查询所有用户
     *
     * @return 用户列表
     */
    List<Blog_User> listUsers();

    /**
     * 根据用户ID查询用户信息
     *
     * @param id 用户ID
     * @return 用户对象
     */
    Blog_User getUser(int id);

    /**
     * 根据用户ID查询用户名字
     *
     * @param id 用户ID
     * @return 用户名字
     */
    String getUserName(int id);

    /**
     * 根据用户名和密码查询用户
     *
     * @param name     用户名
     * @param password 密码
     * @return 用户对象
     */
    Blog_User getOneUser(@Param("name") String name, @Param("password") String password);

    /**
     * 删除用户
     *
     * @param id 用户ID
     */
    void deleteUser(int id);

    /**
     * 根据用户名模糊搜索用户
     *
     * @param name 用户名
     * @return 用户列表
     */
    List<Blog_User> searchUsers(String name);

    /**
     * 创建新用户
     *
     * @param name 用户对象
     */
    int insertUser(@Param("name") String name,@Param("password")String password,@Param("photo")String photo);

    //获取某个用户的所有信息
    Blog_User getUserData(@Param("id") int id);


    /**
     * 更新用户信息
     *
     */
    void updateUser(@Param("name") String name,@Param("password")String password,@Param("id") int id,
                    @Param("phoneNumber")String phoneNumber,
                    @Param("photo")String photo,@Param("testFlag") int testFlag);
}