package com.atguigu.ssm.mapper;

import com.atguigu.ssm.pojo.Essay;
import com.atguigu.ssm.pojo.User_interaction;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface InteractionMapper {

    /**
     * 根据用户id查询用户的互动列表
     *
     * @param id 用户id
     * @return 用户互动列表
     */
    List<User_interaction> list(int id);

    /**
     * 根据用户id查询用户喜欢的文章列表
     *
     * @param userId 用户id
     * @return 用户喜欢的文章列表
     */
    List<User_interaction> likeEssayList(@Param("userId")  int userId);

    /**
     * 根据用户id查询用户互动的文章列表
     *
     * @param userId 用户id
     * @return 用户互动的文章列表
     */
    List<User_interaction> interactionEssayList(@Param("userId") int userId);

    /**
     * 添加用户的互动记录
     *
     * @param userId     用户id
     * @param favoriteId 收藏的文章id
     */
    void addInteraction(@Param("userId") int userId, @Param("favoriteId") int favoriteId);

    /**
     * 删除用户的互动记录
     *
     * @param userId      用户id
     * @param favoriteId  收藏的文章id
     */
    void deleteInteraction(@Param("userId") int userId, @Param("favoriteId") int favoriteId);

    /**
     * 添加用户的喜欢记录
     *
     * @param userId 用户id
     * @param likeId 喜欢的文章id
     */
    void addLike(@Param("userId") int userId, @Param("likeId") int likeId);

    /**
     * 删除用户的喜欢记录
     *
     * @param userId 用户id
     * @param id     喜欢的文章id
     */
    void deleteLike(@Param("userId") int userId, @Param("id") int id);

    /**
     * 查询用户是否已经喜欢过指定的文章
     *
     * @param userId 用户id
     * @param id     文章id
     * @return 若用户已喜欢，返回1；否则返回0
     */
    int likeSelect(@Param("userId") int userId, @Param("id") int id);

    /**
     * 查询用户是否已经互动过指定的文章
     *
     * @param userId 用户id
     * @param id     文章id
     * @return 若用户已互动，返回1；否则返回0
     */
    int interactionSelect(@Param("userId") int userId, @Param("id") int id);

    void deleteInteractionByEssayId(@Param("essayId") int essayId);
}
