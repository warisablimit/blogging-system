package com.atguigu.ssm.mapper;

import com.atguigu.ssm.pojo.Essay;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EssayMapper {

    /**
     * 查询出所有的文章
     *
     * @return 文章列表
     */
    List<Essay> AllEssayList();

    /**
     * 通过点赞、收藏、评论来降序查询文章列表
     *
     * @return 文章列表
     */
    List<Essay> likeFavoriteCommentEssayList();

    /**
     * 通过id查询某篇文章的详细信息
     *
     * @param id 文章id
     * @return 文章对象
     */
    Essay getEssayById(@Param("id") int id);


    // 在对应的 Mapper 接口中添加方法
    List<Essay> likeEssayList(@Param("likeIdList") List<Integer> likeIdList);


    //所有收藏过的文章
    List<Essay> InteractedEssayList(@Param("likeIdList") List<Integer> likeIdList);

    /**
     * 查询某个用户发布的所有文章
     *
     * @param userId 用户id
     * @return 文章列表
     */
    List<Essay> userEssayList(@Param("userId") int userId);

    /**
     * 添加文章
     *
     * @param
     */
    void insertEssay(@Param("author") int author,@Param("authorName") String authorName,@Param("articleTitle") String articleTitle,@Param("articleImg") String articleImg,@Param("articleData") String articleData,@Param("articleContent") String articleContent,
                     @Param("authorPhoto") String authorPhoto);

    /**
     * 修改文章
     *
     * @param
     */
    void updateEssay(@Param("articleTitle") String articleTitle,@Param("articleImg") String articleImg,@Param("articleData") String articleData,@Param("articleContent") String articleContent,@Param("id") int id);
//    updateEssay
    /**
     * 删除文章
     *
     * @param id 文章id
     */
    void deleteEssay(@Param("id") int id);

    /**
     * 通过模糊查询搜索文章
     *
     * @param text 搜索关键词
     * @return 文章列表
     */
    List<Essay> searchEssay(@Param("text") String text);



    // 添加收藏数减一的方法
    void decrementFavoriteCount(Map<String, Object> params);


    //查询点赞和收藏数最高的六个文章
    List<Essay> sexupEssayList();



    void updateAuthorNameByUserId(@Param("userId") int userId,@Param("authorName") String authorName);

}