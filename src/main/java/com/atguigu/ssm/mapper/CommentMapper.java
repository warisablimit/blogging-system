package com.atguigu.ssm.mapper;

import com.atguigu.ssm.pojo.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentMapper {

    /**
     * 获取所有评论
     *
     * @return 评论列表
     */
    List<Comment> listComment();

    /**
     * 根据文章ID获取评论列表
     *
     * @param essay 文章ID
     * @return 评论列表
     */
    List<Comment> listCommentByEssay(@Param("essay") int essay);

    /**
     * 根据用户ID获取评论列表
     *
     * @param user 用户ID
     * @return 评论列表
     */
    List<Comment> listCommentByUser(int user);

    /**
     * 添加评论
     *
     * @param @Param("userId") int userId,@Param("essayId") int essayId,@Param("content") String content,@Param("authorName") String authorName 评论对象
     */
    void addComment(@Param("userId") int userId,@Param("essayId") int essayId,@Param("content") String content,@Param("authorName") String authorName,@Param("authorPhoto")String authorPhoto);

    /**
     * 根据评论ID删除评论
     *
     * @param id 评论ID
     */
    void deleteComment(int id);

    /**
     * 根据用户ID和文章ID删除评论
     *
     * @param userId  用户ID
     * @param essayId 文章ID
     */
    void deleteCommentByUserAndEssay(@Param("userId") int userId, @Param("essayId") int essayId);

    /**
     * 根据评论内容进行搜索匹配
     *
     * @param text 搜索关键词
     * @return 匹配的评论列表
     */
    List<Comment> searchCommentByContent(@Param("text") String text);

    //查询某个评论
    Comment getComment(@Param("userId") int userId,@Param("essayId") int essayId,@Param("content") String content,@Param("authorName") String authorName);

    //
    void essay_id_delete(@Param("essayId") int essayId);


    //更改评论的作者名
    void updateAuthorNameByUserId(@Param("userId") int userId,@Param("authorName") String authorName);

}