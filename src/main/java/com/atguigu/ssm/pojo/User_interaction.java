package com.atguigu.ssm.pojo;

public class User_interaction {
    private int id;
    private int userId;
    private int favoriteId;
    private int likeId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(int favoriteId) {
        this.favoriteId = favoriteId;
    }

    public int getLikeId() {
        return likeId;
    }

    public void setLikeId(int likeId) {
        this.likeId = likeId;
    }

    @Override
    public String toString() {
        return "UserInteraction{" +
                "id=" + id +
                ", userId=" + userId +
                ", favoriteId=" + favoriteId +
                ", likeId=" + likeId +
                '}';
    }
}