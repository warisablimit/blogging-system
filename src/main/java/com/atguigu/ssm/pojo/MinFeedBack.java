//package com.atguigu.ssm.pojo;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//import org.joda.time.DateTime;
//import org.springframework.format.annotation.DateTimeFormat;
//
//import java.sql.Timestamp;
//
//public class MinFeedBack {
//    private int id;
//    private String content;
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private Timestamp gmtCreate;
//    private int ffeedback;
//    private int author;
//    private String authorName;
//    private String huifuobject;
//    public MinFeedBack() {
//    }
//
//    public MinFeedBack(int id, String content, Timestamp gmtCreate, int ffeedback, int author, String authorName, String huifuobject) {
//        this.id = id;
//        this.content = content;
//        this.gmtCreate = gmtCreate;
//        this.ffeedback = ffeedback;
//        this.author = author;
//        this.authorName = authorName;
//        this.huifuobject = huifuobject;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//
//    public Timestamp getGmtCreate() {
//        return gmtCreate;
//    }
//
//    public void setGmtCreate(Timestamp gmtCreate) {
//        this.gmtCreate = gmtCreate;
//    }
//
//    public int getFfeedback() {
//        return ffeedback;
//    }
//
//    public void setFfeedback(int ffeedback) {
//        this.ffeedback = ffeedback;
//    }
//
//    public int getAuthor() {
//        return author;
//    }
//
//    public void setAuthor(int author) {
//        this.author = author;
//    }
//
//    public String getAuthorName() {
//        return authorName;
//    }
//
//    public void setAuthorName(String authorName) {
//        this.authorName = authorName;
//    }
//
//    public String getHuifuobject() {
//        return huifuobject;
//    }
//
//    public void setHuifuobject(String huifuobject) {
//        this.huifuobject = huifuobject;
//    }
//
//    @Override
//    public String toString() {
//        return "MinFeedBack{" +
//                "id=" + id +
//                ", content='" + content + '\'' +
//                ", gmtCreate=" + gmtCreate +
//                ", ffeedback=" + ffeedback +
//                ", author=" + author +
//                ", authorName='" + authorName + '\'' +
//                ", huifuobject='" + huifuobject + '\'' +
//                '}';
//    }
//}
