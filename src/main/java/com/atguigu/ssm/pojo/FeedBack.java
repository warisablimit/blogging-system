//package com.atguigu.ssm.pojo;
//import javax.persistence.*;
//
//import org.springframework.format.annotation.DateTimeFormat;
//
//import java.sql.Timestamp;
//import java.util.Date;
//import java.text.SimpleDateFormat;
//public class FeedBack {
//    private int id;
//    private String content;
////    @Temporal(TemporalType.TIMESTAMP)
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private Timestamp gmtCreate;
//    private int author;
//    private String authorName;
//
//    public FeedBack() {
//    }
//
//    public FeedBack(int id, String content, Timestamp gmtCreate, int author, String authorName) {
//        this.id = id;
//        this.content = content;
//        this.gmtCreate = gmtCreate;
//        this.author = author;
//        this.authorName = authorName;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//
//    public Timestamp getGmtCreate() {
//        return gmtCreate;
//    }
//
//    public void setGmtCreate(Timestamp gmtCreate) {
//        this.gmtCreate = gmtCreate;
//    }
//
//    public int getAuthor() {
//        return author;
//    }
//
//    public void setAuthor(int author) {
//        this.author = author;
//    }
//
//    public String getAuthorName() {
//        return authorName;
//    }
//
//    public void setAuthorName(String authorName) {
//        this.authorName = authorName;
//    }
//
//    @Override
//    public String toString() {
//        return "FeedBack{" +
//                "id=" + id +
//                ", content='" + content + '\'' +
//                ", gmtCreate=" + gmtCreate +
//                ", author=" + author +
//                ", authorName='" + authorName + '\'' +
//                '}';
//    }
//}