package com.atguigu.ssm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Encryption {

    public static String encryptPassword(String password) {
        String encryptedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(password.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            encryptedPassword = no.toString(16);
            while (encryptedPassword.length() < 32) {
                encryptedPassword = "0" + encryptedPassword;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encryptedPassword;
    }

    public static void main(String[] args) {
        String originalPassword = "123456";
        String encryptedPassword = encryptPassword(originalPassword);
        System.out.println("Original Password: " + originalPassword);
        System.out.println("Encrypted Password (MD5): " + encryptedPassword);

        // 模拟用户登录过程
        String userInputPassword = "123456";
        String userInputEncryptedPassword = encryptPassword(userInputPassword);

        if (userInputEncryptedPassword.equals(encryptedPassword)) {
            System.out.println("Login Successful");
        } else {
            System.out.println("Login Failed");
        }
    }
}