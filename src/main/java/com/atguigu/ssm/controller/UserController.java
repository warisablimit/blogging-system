package com.atguigu.ssm.controller;

import com.atguigu.ssm.pojo.Blog_User;
import com.atguigu.ssm.pojo.Essay;
import com.atguigu.ssm.pojo.User_interaction;
import com.atguigu.ssm.service.CommentService;
import com.atguigu.ssm.service.EssayService;
import com.atguigu.ssm.service.InteractionService;
import com.atguigu.ssm.service.UserService;
import com.atguigu.ssm.util.MD5Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {



    @Autowired
    private UserService userService;

    @Autowired
    private InteractionService interactionService;

    @Autowired
    private EssayService essayService;

    @Autowired
    private CommentService commentService;
    //
    @RequestMapping(value = "userpage" , method = RequestMethod.GET)
    public String userpage(@RequestParam("id") int userid, HttpServletRequest request, HttpServletResponse response){
        Blog_User blogUser = userService.getUserData(userid);

        //查询该用户所有的发布过的文章
        List<Essay> essays =essayService.getUserEssayList(userid);
        request.setAttribute("user_list", blogUser);
        request.setAttribute("essays", essays);
        return "user_page";
    }




    @RequestMapping(value = "update_userdata", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateUserData(@RequestParam("id") int userId,
                                              @RequestParam("name") String userName,
                                              @RequestParam("password") String password,
                                              @RequestParam("tel_phone") String phoneNumber,
                                              @RequestParam("photo") String photo,
                                              HttpServletRequest request) {
        Map<String, Object> result = new HashMap<>();
        // 对用户密码进行加密

        System.out.println("用户密码-----------------------" + password + "密码是否为空=="+password.equals("") );
        String encryptedPassword = MD5Encryption.encryptPassword(password);

        // 调用userService的updateUser方法，传入加密后的密码
        if (password.equals("")){
            userService.updateUser(userName, encryptedPassword, userId, phoneNumber,photo,0);
        }else if (!password.equals("")){
            userService.updateUser(userName, encryptedPassword, userId, phoneNumber,photo,1);
        }
        Blog_User blogUser = userService.getUserData(userId);
        HttpSession session = request.getSession(true);
        session.setAttribute("photo", blogUser.getPhoto());
        session.setAttribute("username", blogUser.getName());
        result.put("user", blogUser);
        return result;
    }

    @RequestMapping(value = "user_dongtai" ,method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> UserData(@RequestParam("id") int userId,@RequestParam("method") int method,HttpServletRequest request) {

        Map<String, Object> result = new HashMap<>();
        List<User_interaction> likes = null;
        boolean if_num = false;
        List<Essay> essays = null;
        List<Integer> IntegetList = new ArrayList<>();
        if (method == 1){
            likes = interactionService.listLikedEssays(userId);
            if_num = true;
        }
        else if (method == 2){
            likes = interactionService.listInteractedEssays(userId);
            System.out.println( " ===============================" + likes.size());
            if_num = true;
        }
        else if(method == 3){
            essays =essayService.getUserEssayList(userId);
        }
        if (if_num == true && likes.size() != 0) {
            if (method == 1){
                for (int i = 0; i < likes.size(); i++) {
                    IntegetList.add(likes.get(i).getLikeId());
                }
            }else if(method == 2){
                for (int i = 0; i < likes.size(); i++) {
                    IntegetList.add(likes.get(i).getFavoriteId());
                }
            }
            // 其他处理 likes 列表的代码
            essays = essayService.likeEssayList(IntegetList);
        }


        result.put("essays", essays);
        System.out.println("状态---"+ ResponseEntity.ok(result));
        return result;
    }

    @RequestMapping(value = "user_list",method = RequestMethod.GET)
    public String user_list(HttpServletRequest request){
        List<Blog_User> users = userService.listUsers();
        request.setAttribute("users", users);
        return "pages/page/user_list";
    }

    @RequestMapping(value = "delete_user",method = RequestMethod.GET)
    public String delete_user(@RequestParam("id") int id, HttpServletRequest request){
        userService.deleteUser(id);
        return "redirect:/user_list";
    }


    @RequestMapping(value = "search_user",method = RequestMethod.POST)
    public String search_user(@RequestParam("name") String name, HttpServletRequest request){
        List<Blog_User> users = userService.searchUsers(name);
        request.setAttribute("users_size",users.size());
        request.setAttribute("users", users);
        return "pages/page/search_user";
    }
    @RequestMapping(value = "searchuser_byname",method = RequestMethod.GET)
    public String searchuser_byname( HttpServletRequest request){
        return "pages/page/search_user";
    }
}
