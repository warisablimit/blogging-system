//package com.atguigu.ssm.controller;
//
//import com.atguigu.ssm.pojo.FeedBack;
//import com.atguigu.ssm.pojo.MinFeedBack;
//import com.atguigu.ssm.service.FeedBackService;
//import com.atguigu.ssm.service.MinFeedBackService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//import java.util.Map;
//
//@Controller
//public class FeedBackController {
//
//    @Autowired
//    private FeedBackService feedBackService;
//    @Autowired
//    private MinFeedBackService minFeedBackService;
//
//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public String feedback(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
//        List<FeedBack> feedBacks = feedBackService.FeedBackList();
//        List<MinFeedBack> minFeedBacks = minFeedBackService.MinFeedBackList();
//        request.setAttribute("minFeedBacks", minFeedBacks);
//        request.setAttribute("feedbacks", feedBacks);
//        return "index";
//    }
//
//    @RequestMapping(value = "/insertfeedback",method = RequestMethod.POST)
//    public void insertfeedback(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        String author_name = request.getParameter("author_name");
//        String content = request.getParameter("content");
//        int feedback_id = Integer.parseInt(request.getParameter("feedback_id"));
//        String hiufu_object = request.getParameter("hiufu_object");
//        int author_id = Integer.parseInt(request.getParameter("author_id"));
//        if (feedback_id == 0  || (hiufu_object.equals("") || hiufu_object == null)){
//            int feedBack = feedBackService.insertFeedBack(content,author_id,author_name);
//            response.sendRedirect("/");
//        }else{
//            int feedBack = minFeedBackService.InsertMinFeedBack(content,author_id,author_name,feedback_id,hiufu_object);
//            response.sendRedirect("/");
//        }
////        response.sendRedirect("/");
////        System.out.println(feedBack);
//    }
//
//    @RequestMapping(value = "/deletefeedback", method = RequestMethod.GET)
//    @ResponseBody
//    public void deleteComment(@RequestParam("id") int commentId, HttpServletResponse response) throws IOException {
//        // 处理删除留言的逻辑...
////        int commentId = (int) params.get("id"); // 获取留言的id
//        System.out.println(commentId);
////        首先删除回复的评论
//        deleteMinFeedBacks(commentId);
//        //然后再删除对应的头部评论
//        feedBackService.deleteFeedBack(commentId); // 调用Service和Mapper进行删除操作
//        response.sendRedirect("/");
//    }
//
//    //删除被本留言回复的所有留言
//    public void deleteMinFeedBacks(int commentId){
//        minFeedBackService.deleteMinFeedBacks(commentId);
//    }
//
//    @RequestMapping(value = "/mindeletefeedback", method = RequestMethod.GET)
//    @ResponseBody
//    public void mindeleteComment(@RequestParam("id") int commentId, HttpServletResponse response) throws IOException {
//        // 处理删除留言的逻辑...
////        int commentId = (int) params.get("id"); // 获取留言的id
//        System.out.println(commentId);
//        minFeedBackService.deleteMinFeedBack(commentId); // 调用Service和Mapper进行删除操作
//        response.sendRedirect("/");
//    }
//
//    @RequestMapping(value = "/minfeedback", method = RequestMethod.GET)
//    public String minfeedback(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
//        List<MinFeedBack> minFeedBacks = minFeedBackService.MinFeedBackList();
//        System.out.println("=============================================");
//        System.out.println(minFeedBacks);
////        request.setAttribute("feedbacks", feedBacks);
//        return "index";
//    }
//
//
//}
