package com.atguigu.ssm.controller;

import com.atguigu.ssm.pojo.Blog_User;
import com.atguigu.ssm.pojo.Comment;
import com.atguigu.ssm.pojo.Essay;
import com.atguigu.ssm.service.CommentService;
import com.atguigu.ssm.service.EssayService;
import com.atguigu.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private EssayService essayService;

    @Autowired
    private UserService userService;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    Date date;

    @RequestMapping(value = "addComment", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addComment(@RequestParam("content") String content,
                             @RequestParam("userId") int userId,
                             @RequestParam("userName") String userName,
                             @RequestParam("essayId") int essayId) {
        // 处理评论内容，userId、userName和essayId
        // ...
        Blog_User blog_user = userService.getUser(userId);
        Map<String, Object> params = new HashMap<>();
        commentService.addComment(userId,essayId,content,userName,blog_user.getPhoto());
        params.put("action", 5);
        params.put("id", essayId); // 文章ID
        essayService.decrementFavoriteCount(params);
        Comment comment = commentService.getComment(userId,essayId,content,userName);
        System.out.println();
        Essay essay = essayService.getEssayById(essayId);
        Map<String, Object> result = new HashMap<>();
        result.put("essay", essay);
        result.put("comment", comment);
        System.out.println("comment----------------------------" + comment);
        System.out.println("状态---"+ ResponseEntity.ok(result));
        return result;
    }

    @RequestMapping(value = "comment_list" ,method = RequestMethod.GET)
    public String comment_list(HttpServletRequest request){
        List<Comment> comments = commentService.listComment();
        request.setAttribute("comments", comments);
        return "pages/page/comment_list";
    }

    @RequestMapping(value = "delete_comment",method = RequestMethod.GET)
    public String delete_comment(@RequestParam("id") int id, HttpServletRequest request){
        commentService.deleteComment(id);
        return "redirect:/comment_list";
    }


    @RequestMapping(value = "searchcomment_by_essay",method = RequestMethod.POST)
    public String comment_list_by_essay(@RequestParam("essayId") int essayId, HttpServletRequest request){
        List<Comment> comments = commentService.listCommentByEssay(essayId);
        request.setAttribute("comments", comments);
        request.setAttribute("comment_size", comments.size());
        return "pages/page/search_comment";
    }

    @RequestMapping(value = "searchcomment_by_content",method = RequestMethod.POST)
    public String search_comment(@RequestParam("text") String text, HttpServletRequest request){
        List<Comment> comments = commentService.searchCommentByContent(text);
        request.setAttribute("comments", comments);
        request.setAttribute("comment_size", comments.size());
        return "pages/page/comment_content_search";
    }

    @RequestMapping(value = "searchcomment_byessayid" , method = RequestMethod.GET)
    public String searchcomment_byessayid(HttpServletRequest request){
        return "pages/page/search_comment";
    }


    @RequestMapping(value = "searchcomment_bycontent" , method = RequestMethod.GET)
    public String searchcomment_bycontent(HttpServletRequest request){
        return "pages/page/comment_content_search";
    }

}
