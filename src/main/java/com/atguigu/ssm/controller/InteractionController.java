package com.atguigu.ssm.controller;

import com.atguigu.ssm.pojo.Essay;
import com.atguigu.ssm.service.EssayService;
import com.atguigu.ssm.service.InteractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class InteractionController {

    @Autowired
    private InteractionService interactionService;

    @Autowired
    private EssayService essayService;


    @RequestMapping(value = "/like", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> addlike(@RequestParam("if_data") int if_data,@RequestParam("id") int essayId,
                                       @RequestParam(value = "user_id", required = false) int user_id) {
        Map<String, Object> params = new HashMap<>();
        System.out.println("if_data===============================" + if_data);
        if (if_data == 1){
            interactionService.addLike(user_id, essayId);
        }else if (if_data == 2){
            interactionService.deleteLike(user_id, essayId);
        }
        else if (if_data == 3){
            interactionService.addInteraction(user_id, essayId);
        }
        else if (if_data == 4){
            interactionService.deleteInteraction(user_id, essayId);
        }
        // 这里就是对应点赞和收藏分别表示 1：点赞，2：取消点赞，3：收藏，4：取消收藏；
        params.put("action", if_data);
        params.put("id", essayId); // 文章ID
        essayService.decrementFavoriteCount(params);
        boolean like = interactionService.hasLiked(user_id, essayId);
        boolean interaction = interactionService.hasInteracted(user_id,essayId);
        System.out.println("-----------------------" +like + "--------------------");
//        boolean favorite = interactionService.hasInteracted(userId, essayId);
        Essay essay = essayService.getEssayById(essayId);
        // 构造返回数据
        Map<String, Object> result = new HashMap<>();
        result.put("essay", essay);
        result.put("like", like);
        result.put("interaction", interaction);
        System.out.println("状态---"+ResponseEntity.ok(result));
        return result;
    }






}
