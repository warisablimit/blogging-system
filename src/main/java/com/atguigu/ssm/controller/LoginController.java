package com.atguigu.ssm.controller;

import com.atguigu.ssm.pojo.Blog_User;
import com.atguigu.ssm.service.UserService;
import com.atguigu.ssm.util.MD5Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class LoginController {

//    MD5Encryption md5 = new MD5Encryption();

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/dologin", method = RequestMethod.POST)
    public String dologin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");

        // 对用户密码进行加密
        String encryptedPassword = MD5Encryption.encryptPassword(password);

        Blog_User user = userService.getOneUser(name, encryptedPassword);
        if (user == null){
            response.sendRedirect("/"); // 如果验证成功，重定向到主页
            return null; // 确保方法结束，不会继续执行后面的代码
        } else if (user != null) {
            HttpSession session = request.getSession(true);
            session.setAttribute("photo",user.getPhoto());
            session.setAttribute("userid",user.getId());
            session.setAttribute("username",user.getName());
            session.setAttribute("usertype",user.getType());
            if (user.getType().equals("USER")){
                response.sendRedirect("/"); // 如果验证成功，重定向到主页
            }else if (user.getType().equals("PROUSER")){
                response.sendRedirect("/prouser");
            }
            return null; // 确保方法结束，不会继续执行后面的代码
        }

        response.sendRedirect("/"); // 如果验证成功，重定向到主页
        return null; // 确保方法结束，不会继续执行后面的代码
    }
    @RequestMapping(value = "/enrol", method = RequestMethod.POST)
    public String enrol(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");

        // 检查参数是否为空
        if (name == null || name.trim().isEmpty() || password == null || password.trim().isEmpty()) {
            response.sendRedirect("/"); // 如果参数为空，直接重定向到主页
            return null; // 确保方法结束，不会继续执行后面的代码
        }

        // 对用户密码进行加密
        String encryptedPassword = MD5Encryption.encryptPassword(password);
        String photo = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgZmlsbD0iY3VycmVudENvbG9yIiBjbGFzcz0iYmkgYmktcGVyc29uLWNpcmNsZSIgdmlld0JveD0iMCAwIDE2IDE2Ij4KICA8cGF0aCBkPSJNMTEgNmEzIDMgMCAxIDEtNiAwIDMgMyAwIDAgMSA2IDB6Ii8+CiAgPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBkPSJNMCA4YTggOCAwIDEgMSAxNiAwQTggOCAwIDAgMSAwIDh6bTgtN2E3IDcgMCAwIDAtNS40NjggMTEuMzdDMy4yNDIgMTEuMjI2IDQuODA1IDEwIDggMTBzNC43NTcgMS4yMjUgNS40NjggMi4zN0E3IDcgMCAwIDAgOCAxeiIvPgo8L3N2Zz4=";
        // 调用userService的insertUser方法，并传入加密后的密码
        int user = userService.insertUser(name, encryptedPassword,photo);

        if (user == 1) {
            Blog_User user2 = userService.getOneUser(name, encryptedPassword);
            HttpSession session = request.getSession(true);
            session.setAttribute("photo", user2.getPhoto());
            session.setAttribute("userid", user2.getId());
            session.setAttribute("username", user2.getName());
            response.sendRedirect("/"); // 如果验证成功，重定向到主页
            return null; // 确保方法结束，不会继续执行后面的代码
        }

        response.sendRedirect("/"); // 如果验证成功，重定向到主页
        return null; // 确保方法结束，不会继续执行后面的代码
    }



    @RequestMapping(value = "/logout" , method = RequestMethod.GET)
    public void logout(HttpServletRequest req, HttpServletResponse resp){
        HttpSession session = req.getSession();
        session.invalidate();
        //invalidate的意思是无效
        try {
            resp.sendRedirect("/");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
