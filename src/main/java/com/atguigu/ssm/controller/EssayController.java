package com.atguigu.ssm.controller;

import com.atguigu.ssm.pojo.Blog_User;
import com.atguigu.ssm.pojo.Essay;
import com.atguigu.ssm.service.CommentService;
import com.atguigu.ssm.service.EssayService;
import com.atguigu.ssm.service.InteractionService;
import com.atguigu.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class EssayController {

    @Autowired
    private  EssayService essayService;

    //导入用户操作的Service
    @Autowired
    private InteractionService interactionService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserService userService;
    @RequestMapping(value = "prouser" , method = RequestMethod.GET)
    public String prouser(HttpServletRequest request, HttpServletResponse response){

        return "pages/main";
    }


    @RequestMapping(value = "/" , method = RequestMethod.GET)
    public String index(HttpServletRequest request, HttpServletResponse response){
        List<Essay> essays = essayService.getAllEssayList();
        List<Essay> up_essays = essayService.sexupEssayList();
        request.setAttribute("essays", essays);
        request.setAttribute("up_essays", up_essays);
        return "index";
    }
    @RequestMapping(value = "essay_list" ,method = RequestMethod.GET)
    public String essay_list(HttpServletRequest request){
        List<Essay> essays = essayService.getAllEssayList();
        request.setAttribute("essays", essays);
        return "pages/page/essay_list";
    }

    @RequestMapping(value = "essay_page", method = RequestMethod.GET)
    public String essay_page(@RequestParam("id") int essayId, @RequestParam(value = "user_id", required = false) Integer userId, HttpServletRequest request, HttpServletResponse response) {
        // 检查 userId 是否为 null 或为空字符串
        request.setAttribute("essay", essayService.getEssayById(essayId));
        request.setAttribute("comments", commentService.listCommentByEssay(essayId));
        System.out.println("essay_id---------------" + essayId);
        System.out.println( "user_id---------------" + userId);
        if (userId != null) {
            // 使用essayId和userId进行后续的处理逻辑
            // ...
            boolean like = interactionService.hasLiked(userId, essayId);
            boolean favorite = interactionService.hasInteracted(userId, essayId);
            System.out.println("用户是否点赞");
            request.setAttribute("panduan2", favorite);
            request.setAttribute("panduan", like);
            return "essay";
        } else {
            System.out.println("========================================");
            System.out.println(essayService.getEssayById(essayId));
            return "essay";
        }
    }

    //搜索文章
    @RequestMapping(value = "/search", method = RequestMethod.POST)
//    @ResponseBody
    public String search(@RequestParam(name = "keyword", required = false) String keyword,HttpServletRequest request) {
        request.setAttribute("keyword", keyword);
        List<Essay> essays = essayService.searchEssay(keyword);
        System.out.println("文章的大小---------------------------" + essays.size());
        if (essays.size() == 0) {
            request.setAttribute("essays", essays);
            request.setAttribute("zero_essays", 0);
            return "search";
        } else if (essays.size() >= 1) {
            request.setAttribute("essays", essays);
            return "search";
        }
        return "";
    }

    //
    @RequestMapping(value = "/addessay", method = RequestMethod.GET)
    public String addessay(@RequestParam(name = "id", required = false) String keyword,HttpServletRequest request) {
        return "text";
    }




    @RequestMapping(value = "/add_essay", method = RequestMethod.POST)
    public void add_essay(
            @RequestParam("author") int author,
            @RequestParam("authorName") String authorName,
            @RequestParam("articleTitle") String articleTitle,
            @RequestParam("articleImg") String articleImg,
            @RequestParam("articleData") String articleData,
            @RequestParam("articleContent") String articleContent,
            HttpServletRequest request, HttpServletResponse response) {
        // 处理文章添加的逻辑
        Blog_User blog_user = userService.getUser(author);
        essayService.addEssay(author, authorName, articleTitle, articleImg, articleData, articleContent,blog_user.getPhoto());
        System.out.println("------------------------创建成功");
//        return "/"; // 假设添加成功后跳转到成功页面
    }


    @RequestMapping(value = "/update_essays", method = RequestMethod.POST)
    public void update_essay(
            @RequestParam("articleTitle") String articleTitle,
            @RequestParam("articleImg") String articleImg,
            @RequestParam("articleData") String articleData,
            @RequestParam("articleContent") String articleContent,
            @RequestParam("id") int id,
            HttpServletRequest request, HttpServletResponse response) {
        // 处理文章添加的逻辑
        essayService.updateEssay(articleTitle,articleImg, articleData, articleContent, id);
        System.out.println("------------------------修改成功");
//        return "/"; // 假设添加成功后跳转到成功页面
    }

    @RequestMapping(value = "/delete_essay", method = RequestMethod.GET)
    public void delete_essay(
            @RequestParam("id") int id,
            HttpServletRequest request, HttpServletResponse response) {
        // 处理文章添加的逻辑
        interactionService.deleteInteractionByEssayId(id);
        commentService.essay_id_delete(id);
        essayService.deleteEssay(id);
        System.out.println("------------------------删除成功");
//        return "/"; // 假设添加成功后跳转到成功页面
    }

    @RequestMapping(value = "/delete_essays", method = RequestMethod.GET)
    public String delete_essays(
            @RequestParam("id") int id,
            HttpServletRequest request, HttpServletResponse response) {
        // 处理文章添加的逻辑
        interactionService.deleteInteractionByEssayId(id);
        commentService.essay_id_delete(id);
        essayService.deleteEssay(id);
        return "redirect:/essay_list";
//        return "/"; // 假设添加成功后跳转到成功页面
    }

    //修改文章
    @RequestMapping(value = "/update_essay" , method = RequestMethod.GET)
    public String update_essay(@RequestParam("id") int id, HttpServletRequest request, HttpServletResponse response){
        request.setAttribute("essay", essayService.getEssayById(id));
        return "update_essay";
    }


    @RequestMapping(value = "search_page", method = RequestMethod.GET)
    public String search_page(HttpServletRequest request) {
        return "pages/page/search_essay";
    }

    @RequestMapping(value = "/search_essay_pro", method = RequestMethod.POST)
//    @ResponseBody
    public String search_essay_pro(@RequestParam(name = "text", required = false) String keyword,HttpServletRequest request) {
        List<Essay> essays = essayService.searchEssay(keyword);
        request.setAttribute("essay_size", essays.size());
        request.setAttribute("essays", essays);
        return "redirect:essay_list";
    }
}
