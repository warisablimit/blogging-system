<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/7/5
  Time: 17:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>富文本编辑器</title>
    <link href="https://unpkg.com/@wangeditor/editor@latest/dist/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/text.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link href="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <style>
        #editor—wrapper {
            /*             box-sizing: border-box !important; */
            padding: 10px;
            width: 700px !important;
            margin: auto !important;
            /*border: 1px solid #ccc;*/
            background-color: white;
            box-shadow: 0px 0px 5px rgba(0,0,0,0.15);
            z-index: 100; /* 按需定义 */
        }
        #toolbar-container { border-bottom: 1px solid #ccc; }
        #editor-container {
            box-sizing: border-box !important;
            width: 100% !important;
            border-radius: 10px;
            height: 680px;
        }
        [data-menu-key="group-video"] {
            display: none !important;
        }
    </style>
</head>
<body>
<div class="write_essay">
    <div id="editor—wrapper">
        <form method="post">
            <div class="essay_topImg">
                <img id="previewImage" src="#" alt="头部图片">
            </div>
            <input style="display: none;" type="text" value="${sessionScope.username}" name="authorName">
            <div class="write_top">
                <div class="essay_title">
                    <input type="text" class="essay_title_input" name="articleTitle" placeholder="请输入文章的标题" >
                </div>
                <div class="post_image">
                    <label for="fileInput" class="file-input-wrapper">
                        上传图片
                        <input type="file" id="fileInput">
                    </label>
                </div>
                <div class="essay_datatext">
                    <textarea id="textarea" name="articleData" placeholder="请输入文章摘要，文章的主要内容汇总" rows="2" maxlength="100"></textarea>
                </div>

                <textarea style="display: none" name="articleImg" id="base64TextArea" rows="10" cols="50"></textarea>
            </div>
            <div id="toolbar-container"><!-- 工具栏 --></div>
            <div id="editor-container" style="border-radius: 10px !important;"><!-- 编辑器 --></div>
            <div class="hidden_div">
                <textarea name="articleContent" id="content_essay"></textarea>
            </div>
            <div class="submit_div">
                <input type="button" value="发布文章" class="submit_name">
            </div>
        </form>
    </div>
</div>
<div class="toast-container position-fixed bottom-0 end-0 p-3">
    <div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body" style="color: rgb(94, 152, 255) !important;font-weight: 700">
                请输入文章标题
            </div>
            <button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>
</div>
</body>
<script src="https://unpkg.com/@wangeditor/editor@latest/dist/index.js"></script>
<script>
    const { createEditor, createToolbar } = window.wangEditor

    const editorConfig = {
        placeholder: '请在这里输入正文....',
        onChange(editor) {
            const html = editor.getHtml()
            // console.log(editor.getHtml())
            // 获取 textarea 的值
            var textareaValue = $("#content_essay").val();

            textareaValue = editor.getHtml()
            $("#content_essay").val(textareaValue)
            console.log(textareaValue)
            //   console.log('editor content', html)
            // 也可以同步到 <textarea>
        },
        MENU_CONF: {}
    }
    editorConfig.MENU_CONF['uploadImage'] = {
        // 其他配置...

        // 小于该值就插入 base64 格式（而不上传），默认为 0
        base64LimitSize: 600 * 1024,  // 5kb
    }

    const content_essay = document.querySelector("#content_essay")

    const editor = createEditor({
        selector: '#editor-container',
        html: '<p><br></p>',
        config: editorConfig,
        mode: 'default', // or 'simple'
        // onChange(editor) {
        //     const html = editor.getHtml()
        //     document.getElementById('editor-content-view').innerHTML = html
        //     document.getElementById('editor-content-textarea').value = html
        // }
    })

    const toolbarConfig = {}

    const toolbar = createToolbar({
        editor,
        selector: '#toolbar-container',
        config: toolbarConfig,
        mode: 'default', // or 'simple'
    })


    // const input_title = document.querySelector(".essay_title_input");
    // const title = document.querySelector(".title_p")
    // input_title.addEventListener("input", function() {
    //     title.innerHTML = input_title.value
    // });



    //上传头部照片
    function handleFile() {
        const fileInput = document.getElementById("fileInput");
        const essay_topImg = document.querySelector(".essay_topImg");
        const file = fileInput.files[0];
        const reader = new FileReader();

        // 判断文件大小是否超过600KB
        if (file.size > 1 * 1024 * 1024) {
            $(".toast-body").html("图片大小不能超过600KB");
            return;
        }

        reader.onload = function(event) {
            const base64String = event.target.result;
            previewImage.src = base64String;
            base64TextArea.value = base64String;
            essay_topImg.style.display = 'block';
        };

        reader.readAsDataURL(file);
    }
    document.getElementById('fileInput').addEventListener('change', handleFile);


    //这里就是判断用户是否进行正确的填写了必要的内容
    document.addEventListener('DOMContentLoaded', function() {
        var inputs = document.querySelectorAll('input, textarea');
        var validationMessage = document.getElementById('validationMessage');
        inputs.forEach(function(input) {
            input.addEventListener('input', function() {
                if (input.value.trim() === '') {
                    input.style.color = 'red'; // 将placeholder文字改成红色
                } else {
                    input.style.color = 'initial'; // 将placeholder文字恢复正常颜色
                }
            });
        });

        var form = document.querySelector('.submit_name');
        form.addEventListener('click', function() {
            var essayTitle = $('.essay_title_input').val().trim();
            var essayData = $('#textarea').val().trim();
            var imgSrc = $('#base64TextArea').val().trim();
            var essayContent = $('#content_essay').val().trim();
            var authorName = "${sessionScope.username}"; // 获取作者名字
            var authorId = "${sessionScope.userid}";//获取作者的id
            var toastElList = [].slice.call(document.querySelectorAll('.toast'))
            var toastList = toastElList.map(function(toastEl) {
                return new bootstrap.Toast(toastEl)
            })
            if (essayTitle === '' || essayData === '' || imgSrc === '' || essayContent === '') {
                // validationMessage.innerText = '请填写所有必要的内容！';
                if (essayTitle === ''){
                    $(".toast-body").html("请输入文章标题");
                }else if (essayData === ''){
                    $(".toast-body").html("请输入文章的摘要");
                }else if (imgSrc === ''){
                    $(".toast-body").html("请上传文章的头部图片");
                }else if (essayContent === ''){
                    $(".toast-body").html("请输入文章的内容");
                }
                toastList.forEach(toast => toast.show())
            } else {
                //发送ajax请求到后端
                console.log(authorId)
                console.log(essayTitle)
                console.log(essayData)
                console.log(imgSrc)
                console.log(essayContent)
                console.log(authorName)
                $(".toast-body").html("发布成功");
                $.ajax({
                    type: 'POST',
                    url: '/add_essay',
                    data: {
                        author: authorId,
                        authorName: authorName,
                        articleTitle: essayTitle,
                        articleImg: imgSrc,
                        articleData: essayData,
                        articleContent: essayContent
                    },
                    success: function(response) {
                        // 请求成功的处理逻辑

                        console.log('数据提交成功');
                        // 延迟三秒后跳转
                        setTimeout(function() {
                            window.location.href = '/';
                        }, 3000);
                    },
                    error: function() {
                        // console.log(this.data);
                        // 其他处理逻辑
                        $(".toast-body").html("发布失败");
                        console.log('数据提交失败');
                    }
                });
                toastList.forEach(toast => toast.show())
            }

        });
    });

</script>
</html>