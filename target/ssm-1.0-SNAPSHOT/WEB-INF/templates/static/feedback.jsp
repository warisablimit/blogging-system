<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/11/20
  Time: 10:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
<jsp:include page="static/pagetop.jsp"></jsp:include>

<div class="max_div">
    <%--    <a href="/minfeedback">获取</a>--%>
    <div class="user_name" style="display: none">
        ${sessionScope.username}
        <%--        ${sessionScope.userid}--%>
    </div>
    <c:forEach items="${feedbacks}" var="feedback">
        <div class="max_message">
            <div class="message_div">
                <!-- 在这里编写 message_div 元素的内容 -->
                <div class="message_content" id="message${feedback.id}">
                    <!-- 设置内容 -->
                        ${feedback.content}
                </div>
                <div class="message_information">
                    <div class="author_img">
                        <img src="/image/鱼聪明AI绘画.jpg" alt="${feedback.authorName}的头像" title="${feedback.authorName}的头像" style="width: 100%;height: 100%;">
                    </div>
                    <div class="author_name" id="user_name${feedback.id}">${feedback.authorName}</div>
                    <div class="cover_message" data-target="message${feedback.id}">
                        放大
                    </div>
                    <div class="hiufu" data-target="user_name${feedback.id}" id="replyButton${feedback.id}">
                        回复
                    </div>
                    <c:if test="${sessionScope.userid == feedback.author}">
                        <div class="delete">
                            <a href="/deletefeedback?id=${feedback.id}">删除</a>
                        </div>
                    </c:if>
                    <div class="message_time">${feedback.gmtCreate}</div>
                        <%--                    <div class="message_time">2023-11-22 00:20:23</div>--%>
                    <!-- 其他元素，根据需要添加 -->
                </div>
                    <%--                minFeedBacks--%>


            </div>
            <c:forEach items="${minFeedBacks}" var="minFeedBacks">
                <c:if test="${feedback.id == minFeedBacks.ffeedback}">
                    <div class="min_message">
                        <div class="min_message_div">
                            <div class="author_objectauthor">
                                    ${minFeedBacks.authorName} 回复 ${minFeedBacks.huifuobject}
                            </div>
                            <div class="message_content min_message_content" id="min_message${minFeedBacks.id}">
                                    ${minFeedBacks.content}
                            </div>
                            <div class="min_message_information" >
                                <div class="min_author_img">
                                    <img src="/image/鱼聪明AI绘画.jpg" alt="${feedback.authorName}的头像" title="${feedback.authorName}的头像" style="width: 100%;height: 100%;">
                                </div>
                                <div class="min_author_name author_name" id="min_user_name${minFeedBacks.id}">${minFeedBacks.authorName}</div>
                                <div class="min_cover_message cover_message" data-target="min_message${minFeedBacks.id}">
                                    放大
                                </div>
                                <div class="min_hiufu hiufu" data-target="min_user_name${minFeedBacks.id}" id="replyButton${feedback.id}">
                                    回复
                                </div>
                                <c:if test="${sessionScope.userid == minFeedBacks.author}">
                                    <div class="delete">
                                        <a href="/mindeletefeedback?id=${minFeedBacks.id}">删除</a>
                                    </div>
                                </c:if>
                                <div class="min_message_time">
                                        ${minFeedBacks.gmtCreate}
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
            </c:forEach>
        </div>
    </c:forEach>
</div>
<c:if test="${not empty sessionScope.userid}">
    <div class="bottom_form">
        <div class="from_div">
            <div class="author_name_huifu"></div>
            <form action="/insertfeedback" method="post" class="message_from">
                <div class="athor_name_hideen" style="display: none">
                    <input type="text" name="author_name" value="${sessionScope.username}">
                </div>
                <div class="feedback_id_hideen" style="display: none">
                    <input class="feedback_id_hideen_input" type="text" value="0" name="feedback_id">
                </div>
                <div class="hiufu_object_hideen" style="display: none">
                    <input class="hiufu_object_hideen_input" type="text" name="hiufu_object">
                </div>
                <div class="athor_id_hideen" style="display: none">
                    <input type="text" name="author_id" value="${sessionScope.userid}">
                </div>
                <div class="input">
                    <textarea class="textarea" name="content"  placeholder="请你输入你的留言内容..." rows="2" maxlength="10000"></textarea>
                </div>
                <div class="from_submit">
                    <button class="login_button2" type="submit">发布</button>
                </div>
            </form>
        </div>
    </div>
</c:if>
</body>
<script>
    var coverDivs = document.querySelectorAll(".cover_message");
    var messageContents = document.querySelectorAll(".message_content");
    var isClicked = false;
    var originalHeight = "auto"; // 原始高度
    var newHeight = "50px"; // 新高度

    coverDivs.forEach(function(coverDiv) {
        coverDiv.onclick = function() {
            var targetId = coverDiv.getAttribute("data-target"); // 获取关联的 message_content 的 ID
            var targetMessageContent = document.getElementById(targetId); // 获取关联的 message_content 元素
            if (isClicked) {
                targetMessageContent.style.cursor = 'pointer';
                targetMessageContent.style.height = newHeight; // 设置新高度
            } else {
                targetMessageContent.style.cursor = 'default';
                targetMessageContent.style.height = originalHeight; // 恢复原始高度
            }
            isClicked = !isClicked; // 切换点击状态
        };
    });
    const textarea_from  = document.querySelector('.textarea');
    const bottom_form  = document.querySelector('.bottom_form');
    const from_submit  = document.querySelector('.from_submit');
    const input_textarea  = document.querySelector('.input');
    textarea_from.addEventListener('focus',function(){
        var maxLength = textarea_from.value
        console.log(maxLength.length)
        if (maxLength.length > 500){
            textarea_from.style.height = '500px';
            bottom_form.style.height = '600px';
            textarea_from.style.padding = '40px';
        }else{
            textarea_from.style.height = '150px';
            bottom_form.style.height = '200px';
            textarea_from.style.padding = '40px';
        }
        from_submit.style.display = 'block';
    })
    textarea_from.addEventListener('blur',function(){
        console.log("点击了")
        if (textarea_from.value == "") {
            console.log("不空")
            from_submit.style.display = 'none';
        } else{
            console.log(textarea_from.value)
            from_submit.style.display = 'block';
        }
        bottom_form.style.height = '70px';
        textarea_from.style.padding = '0';
        textarea_from.style.height = '100%';

    })
    var isClicked2 = false;
    var hiufu = document.querySelectorAll(".hiufu");
    var author_name_huifu = document.querySelector(".author_name_huifu")
    var feedback_id_hideen = document.querySelector(".feedback_id_hideen_input")
    var hiufu_object_hideen_input = document.querySelector(".hiufu_object_hideen_input")
    const user_name = document.querySelector(".user_name")
    hiufu.forEach(function(huifu_one) {
        huifu_one.onclick = function() {
            var targetId2 = huifu_one.getAttribute("data-target");
            var messageId = huifu_one.getAttribute("id").replace("replyButton", "");
            console.log("Message ID: " + messageId);
            const textarea_from_content = document.getElementById(targetId2);

            console.log("回复对象：" + textarea_from_content.innerHTML)
            if (isClicked2) {
                feedback_id_hideen.value = messageId;
                hiufu_object_hideen_input.value = textarea_from_content.innerHTML;
                author_name_huifu.innerHTML = '给 '.concat(textarea_from_content.innerHTML, ' 回复：');
                textarea_from.placeholder = '给 '.concat(textarea_from_content.innerHTML, ' 回复：');
            } else {
                feedback_id_hideen.value = 0;
                hiufu_object_hideen_input.value = null;
                textarea_from.placeholder = "请你输入你的留言内容..."
                author_name_huifu.innerHTML = ''.concat(user_name.innerHTML,':');
            }
            isClicked2 = !isClicked2; // 切换点击状态
        }
    });
    // $(document).ready(function() {
    //     $('.delete').click(function() {
    //         var clickedDiv = $(this); // 获取当前点击的div元素
    //         var commentId = clickedDiv.data('id'); // 获取点击的div元素的data-id属性值
    //         console.log(commentId)
    //         // 发送Ajax请求删除留言
    //         $.ajax({
    //             url: '/deletefeedback', // 目标URL，与后端的@RequestMapping注解中的value值对应
    //             type: 'POST', // 请求类型，与后端的@RequestMapping注解中的method值对应
    //             data: { id: commentId }, // 需要发送的数据，将留言的ID作为参数传递
    //             error: function(xhr, status, error) {
    //                 // 请求失败后的回调函数
    //                 console.log('删除失败：' + error);
    //                 // 可以在这里添加其他的错误处理逻辑
    //             }
    //         });
    //     });
    // });
</script>
</html>


