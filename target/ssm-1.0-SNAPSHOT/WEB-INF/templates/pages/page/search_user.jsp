<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/7/10
  Time: 12:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/7/10
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>查询用户</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/static/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/static/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/static/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/static/adminlte/bower_components/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="/static/adminlte/bower_components/dist/css/skins/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- 引入样式 -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <!-- 引入组件库 -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .search_div{
            box-sizing: border-box;
            width: 100%;
            height: 40px;
            padding-left: calc(50% - 165px);
            margin-bottom: 30px;
        }
        .search_input{
            width: 250px;
            height: 40px;
            outline: #00a7d0 !important;
        }
        .search_button{
            margin-bottom: 3px;
            border: 0 !important;
            width: 80px !important;
            height: 40px !important;
        }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <jsp:include page="../common/header.jsp"></jsp:include>

    <jsp:include page="../common/mune.jsp"></jsp:include>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                用户查询页面
                <%--<small>Optional description</small>--%>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <!--------------------------
              | Your Page Content Here |
              -------------------------->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <%--<h3 class="box-title">Bordered Table</h3>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-header">
                            <div class="search_div">






                                <form action="/search_user" method="post">
                                    <input class="search_input" name="name" type="text" placeholder="请输入用户名或用户ID查询">
                                    <input type="submit" class="btn btn-success search_button" value="搜索">
                                </form>






                            </div>
                            <h3>总共查询出 ${users_size} 位用户</h3>
                            <table class="table table-hover" style="">
                                <thead>
                                <tr>
                                    <th scope="col">用户ID</th>
                                    <th scope="col">用户名</th>
                                    <th scope="col">用户密码</th>
                                    <th scope="col">用户类型</th>
                                    <th scope="col">手机号</th>
                                    <th scope="col">身份证号</th>
                                    <th scope="col">修改时间</th>
                                    <th scope="col">注册时间</th>
                                    <th scope="col">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${users}" var="user">
                                    <tr>
                                        <th scope="row">${user.id}</th>
                                        <td>${user.name}</td>
                                        <td>${user.password}</td>
                                        <td>${user.type}</td>
                                        <td>${user.phoneNumber}</td>
                                        <td>${user.idNumber}</td>
                                        <td>${user.gmtModified}</td>
                                        <td>${user.gmtCreate}</td>
                                        <td>
                                            <a href="/delete_user?id=${user.id}"><button type="button" class="btn btn-danger btn-xs">删除</button></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="/static/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/static/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/static/adminlte/bower_components/dist/js/adminlte.min.js"></script>

</body>
</html>