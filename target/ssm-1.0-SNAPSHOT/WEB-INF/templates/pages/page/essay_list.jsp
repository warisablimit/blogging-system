<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 22231
  Date: 2023/7/10
  Time: 10:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>文章列表</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/static/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/static/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/static/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/static/adminlte/bower_components/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="/static/adminlte/bower_components/dist/css/skins/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- 引入样式 -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <!-- 引入组件库 -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <jsp:include page="../common/header.jsp"></jsp:include>

    <jsp:include page="../common/mune.jsp"></jsp:include>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                文章列表
                <%--<small>Optional description</small>--%>
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <!--------------------------
              | Your Page Content Here |
              -------------------------->
<%--            "/pages/student/search_essay.jsp"--%>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <%--<h3 class="box-title">Bordered Table</h3>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-header">
                            <table class="table table-hover" style="">
                                <thead >
                                <tr>
                                    <th scope="col">文章id</th>
                                    <th scope="col">作者名</th>
                                    <th scope="col" style="width: 150px">文章标题</th>
                                    <th scope="col" style="width: 300px">文章摘要</th>
                                    <th scope="col">点赞数</th>
                                    <th scope="col">收藏数</th>
                                    <th scope="col">评论数</th>
                                    <th scope="col">发布时间</th>
                                    <th scope="col">修改时间</th>
                                    <th scope="col">操作</th>
                                </tr>
                                </thead>
                                <tbody id="articleList">
                                <c:forEach items="${essays}" var="essay">
                                    <tr>
                                        <td>${essay.id}</td>
                                        <td>${essay.authorName}</td>
                                        <td>${essay.articleTitle}</td>
                                        <td>${essay.articleData}</td>
                                        <td>${essay.like}</td>
                                        <td>${essay.favoriteId}</td>
                                        <td>${essay.comments}</td>
                                        <td>${essay.gmtCreate}</td>
                                        <td>${essay.gmtModified}</td>
                                        <td>
                                            <a href="/delete_essays?id=${essay.id}"><button type="button" class="btn btn-danger btn-xs">删除</button></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->
<script>

    //点击删除按钮
//     function delete_essay(id){
//         $.ajax({
//             type: "GET",
//             url: "/delete_essays",
//             data: {
//                 id: id
//             },
//             success : function(response) {
//                 // 清空原有文章列表内容
// // 清空原有文章列表内容
//                 console.log("成功")
//             },
//             error: function() {
//                 console.log("发生错误");
//             }
//         });
//     }
</script>
<!-- jQuery 3 -->
<script src="/static/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/static/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/static/adminlte/bower_components/dist/js/adminlte.min.js"></script>

</body>
</html>
