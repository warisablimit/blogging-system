
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<%--<link href="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/css/bootstrap.min.css" rel="stylesheet">--%>
<%--<script src="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/js/bootstrap.bundle.min.js"></script>--%>
<!-- Main Header -->
<header class="main-header" >

    <!-- Logo -->
    <a href="/prouser" style="background-color: #00538e !important;" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">后台</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">简简博客-管理系统</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" style="background-color: #087df6 !important;box-shadow: 0px 0px 8px 3px rgba(0, 0, 0, 0.15);" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="${sessionScope.photo}" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">${sessionScope.username}</span>
                        <span style="display: none;" class="user_if">${sessionScope.usertype}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="${sessionScope.photo}" class="img-circle" alt="User Image">

                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">设置</a>
                            </div>
                            <div class="pull-right">
                                <a href="/logout" class="btn btn-default btn-flat">退 出</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<script>
    const user = document.querySelector(".user_if");

    if (user.innerHTML.trim() === 'USER' || user.innerHTML.trim() === '') {
        window.location.href = "/logout";
    }
</script>
