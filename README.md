# blogging_system

#### 介绍
SSM框架（Spring+SpringMVC+MyBatis）编写的博客系统，多人博客系统，功能简洁初学spring框架者友好,项目当前有对博客进行点赞收藏&评论等功能。个人主页里可以查看发布的文章还有点赞和收藏的文章。

#### 软件架构
- 后端：SSM框架Spring+SpringMVC+MyBatis
- 服务器：Tomcat 9.074
- 前端：暂时没有
- 数据库： MySQL 8.0
- 富文本编辑器： [wangeditor](https://www.wangeditor.com/)
- 前后端没有分离！！！


#### 安装教程
1. 利用gitee来下载项目的zip文件[blogging_system ](https://gitee.com/warisablimit/blogging-system/repository/archive/master.zip)
2. 下载对应的数据库设计[SQL
](https://gitee.com/warisablimit/blogging-system/blob/master/blogsystem.sql)
3. 下载Tomcat 环境应该9.0 以上就可以，我使用的版本是9.074,为了正常运行本项目请使用9.074版本,可以从清华镜像中找到。

#### 使用说明
本系统主要是多人进行发布自己的博客,当前图片上传是利用了base64格式存储后面大家可以进行自己需求配置腾讯云或者阿里云进行设置图片库，这样对数据库也更轻松，当前因为都是base64格式所以图片大小有严格的大小限制，还有关注和粉丝等功能大家也可以添加，也进行扩充功能。

1. 首页
![首页](https://foruda.gitee.com/images/1707743053322659507/4569bb74_11752014.png "首页.png")
2. 个人主页
![个人主页
](https://foruda.gitee.com/images/1707743075833885794/c0094e96_11752014.png "个人主页.png")
3. 文章页面
![文章页面](https://foruda.gitee.com/images/1707743537744552872/00959b32_11752014.png "文章页面.png")
4. 后台管理
![后台管理](https://foruda.gitee.com/images/1707743181419605752/f5765502_11752014.png "后台管理.png")
5. 登录页面
![登录页面](https://foruda.gitee.com/images/1707743319244101517/e870a218_11752014.png "登录页面.png")
6. 文章评论区
![文章评论区](https://foruda.gitee.com/images/1707743603475045543/eb06cf93_11752014.png "评论区.png")
7. 文章编辑页面
![文章编辑页面](https://foruda.gitee.com/images/1707743216454023989/9a9a2955_11752014.png "编写页面.png")
8. 搜索页面
![搜索页面](https://foruda.gitee.com/images/1707743243712742312/2f57aebb_11752014.png "搜索结果（关键词回形针）.png")

项目能适配手机端！

本项目可能会有一些bug本人还没黑盒测试，只是简单的白盒测试了以下，功能基本正常能使用，大佬们多给一些建议，谢谢 :laughing: 

给学者提供扩充功能：
1. 关注&粉丝
2. 消息推送
3. 关注博主更新模块
4. 共创模块也就是上市周边产品这个可以考虑考虑
5. 讨论模块
6. 系统反馈模块

这些也是我暂时想到的模块我会后期更新本项目添加这些模块！

#### 参与贡献
本项目有本人自己独立完成，系统只是简单的博客系统，我喜欢简约设计所以设计简单，沉浸式阅读设计，系统有很多扩展空间所以学者可以自己的需求进行扩展！
若有给我建议的请给2223137632@qq.com 发送信息 :rose: 。
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
